<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    protected $fillable = ['email','token'];
    public $timestamps = false;
    public $primaryKey = 'email';
    public $incrementing = 'false';

    public static function getEmailByConfirmationCode($token)
    {
        return static::where('token', $token)
            ->value('email');

    }


}
