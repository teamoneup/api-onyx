<?php
namespace App\Socket;

require("Connection.php");

//include 'dbconnect.php';
class SocketWork
{
    public function startBinding()
    {
        $ip_address = "128.199.232.1";
//        $ip_address = "192.168.0.109";
        $port = "55535";
    // open a server on port 7331
        $server = stream_socket_server("tcp://$ip_address:$port", $errno, $errorMessage);
        if ($server === false) {
            die("stream_socket_server error: $errorMessage");
        }
        $client_sockets = array();
        while (true) {
            // prepare readable sockets
            $read_sockets = $client_sockets;
            $read_sockets[] = $server;
            // start reading and use a large timeout
            if(!stream_select($read_sockets, $write, $except, 300000)) {
                die('stream_select error.');
            }
            // new client
            if(in_array($server, $read_sockets)) {
                $new_client = stream_socket_accept($server);
                if ($new_client) {
                    //print remote client information, ip and port number
                    echo 'new connection: ' . stream_socket_get_name($new_client, true) . "\n";
                    $client_sockets[] = $new_client;
                    echo "total clients: ". count($client_sockets) . "\n";
                    // $output = "hello new client.\n";
                    // fwrite($new_client, $output);
                }
                //delete the server socket from the read sockets
                unset($read_sockets[ array_search($server, $read_sockets) ]);
            }
            print_r($read_sockets);

            // message from existing client
            foreach ($read_sockets as $socket) {

                $data = fread($socket, 128);

                echo "data: " . $data . "\n";

                $inputArray = explode('(', $data);
                $lengthOfInput = count($inputArray);
                for ($i = 0; $i < $lengthOfInput; $i++) {
//            print_r($inputArr[$i]);

                    $input = $inputArray[$i];
                    echo $input."\n";
                    date_default_timezone_set("Asia/Kolkata");
                    if ($input != '') {
                        $RepondType = substr($input, 12, 2);
                        echo 'found status : ' . $RepondType . '\n';
                        if ($RepondType == "BR") {
                            echo "It is a valid string because Response type is  " . $RepondType . "\n";

                            //get Imei
                            $IMEI = substr($input, 0, 12);
                            echo "Identifier : " . $IMEI . "\n";

                            //get date in formet
                            $date = substr($input, 16, 6);
                            $yy = substr($date, 0, 2);
                            $mm = substr($date, 2, 2);
                            $dd = substr($date, 4, 2);
                            $dateInFormat = '20' . $yy . '-' . $mm . '-' . $dd;
                            echo "Date : " . $date . "\n";
                            echo "Date in format: " . $dateInFormat . "\n";

                            //get Latitude
                            $latDegree = substr($input, 23, 2);
                            $latDecimal = substr($input, 25, 7);
                            $accurateDecimal = round($latDecimal / 60, 8);
                            $RemoveZeroBeforeDot = substr($accurateDecimal, 1, 9);
                            $latInDDF = $latDegree . $RemoveZeroBeforeDot;
                            echo "Latitude : " . $latInDDF . "\n";

                            //get Longitude
                            $longDegree = substr($input, 34, 2);
                            $longDecimal = substr($input, 36, 7);
                            $accurateDecimalLng = round($longDecimal / 60, 8);
                            $RemoveZeroBeforeDotLng = substr($accurateDecimalLng, 1, 9);
                            $longInDDF = $longDegree . $RemoveZeroBeforeDotLng;
                            echo "Longitude : " . $longInDDF . "\n";

                            //get speed
                            $speed = substr($input, 44, 5);
                            echo "Speed : " . $speed . "\n";

                            //get time in format
                            $time = substr($input, 49, 6);

                            $hh = substr($time, 0, 2);
                            $mi = substr($time, 2, 2);
                            $ss = substr($time, 4, 2);
                            $TimeInFormat = $hh.':'.$mi.':'.$ss;
                            echo "Time : ". $time ."<br>";
                            $timeInTwelveHourFormat = date('h:i:s A', strtotime($TimeInFormat));

                            echo "Time in format : ". $timeInTwelveHourFormat ."<br>";

                            //get Distance In Km
                            $distance = substr($input, 70, 8);
                            $distanceInKm = round($distance / 0.6214, 3);
                            echo "Distance In Km : " . $distanceInKm . "\n";

//
                            if( $IMEI != null && $speed != null && $latInDDF != null && $longInDDF != null && $dateInFormat != null && $timeInTwelveHourFormat != null)
                            {
                                //create con object of connection class
                                $con = new Connection();
//                              //call connect function for
                                $con->connect($IMEI, $speed, $latInDDF, $longInDDF, $distanceInKm, $dateInFormat, $timeInTwelveHourFormat);
                            }
                            else
                            {
                                echo "Somthing Missing thats why data not save \n";
                            }

                        } elseif ($RepondType == "BP") {
                            fwrite($socket, '(027043620459AP01HSO)'); //  Outputs 'String? ' without a Line Ending

//                    SocketServer::socket_write_smart($client->socket, "(027043620459AP01HSO)", "\n");
                            echo 'msg sent to client  : (027043620459AP01HSO) '."\n";
                        } else {
                            echo 'Wrong Device trying to connect';
                        }
                    } else {
                        echo "Sorry no data found !" . $input;
                    }
                }

                if (!$data) {
                    unset($client_sockets[ array_search($socket, $client_sockets) ]);
                    @fclose($socket);
                    echo "client disconnected. total clients: ". count($client_sockets) . "\n";
                    continue;
                }
                print_r(is_object($socket));
//        print_r('responce :'.$response);
//        //send the message back to client
//        if (sizeof($response) > 0) {
//            fwrite($socket, 'hello');
//        }
            }
        } // end while loop

    }
}

class SocketInstance extends SocketWork
{
    private static $instance = NULL;

    static public function getInstance()
    {
        if (self::$instance === NULL)
            self::$instance = new SocketInstance();
        return self::$instance;
    }
}
//
$instance = SocketInstance::getInstance();
$instance->startBinding();