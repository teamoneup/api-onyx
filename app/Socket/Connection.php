<?php
namespace App\Socket;

//use  App\Socket\SocketServers;
use PDO;
use PDOException;

class Connection
{

    function connect($imei_number, $speed_limit, $lat, $lon, $vehicle_bearing, $lastDate, $lastTime)
    {
//        var_dump(getenv('DB_DATABASE'));
//        var_dump(getenv('DB_USERNAME'));

//        $deviceTimestamp = $timestamp->format('U = Y-m-d H:i:s');

        try {
            $conn = new PDO("mysql:host=" . getenv('DB_HOST') . ";dbname=".getenv('DB_DATABASE')."",  getenv('DB_USERNAME') , getenv('DB_PASSWORD'));
//            $conn = new PDO("mysql:host=localhost;dbname=onyx", 'root' , '');

            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // query for update data in vehicles table
             $update_location_date_times = $lastDate . ' At ' . $lastTime;
            $sql = "UPDATE vehicles
                    SET current_lat=?,
                        current_long=?,
                        update_location_date_time=?
                        WHERE imei_number=?";

            // Prepare statement
            $stmt = $conn->prepare($sql);

            // execute the query
            $stmt->execute(array($lat, $lon,$update_location_date_times, $imei_number));
            echo $stmt->rowCount() . " records UPDATED successfully";


            // query for insert data into vehicle_histories table
            $vehicleHistories = "INSERT INTO vehicle_histories (imei_number,longitude,latitude,speed_limit,vehicle_bearing,last_date,last_time)
                              VALUES (:imei_number,:longitude,:latitude,:speed_limit,:vehicle_bearing,:last_date,:last_time)";
            $queryForVehicleHistories = $conn->prepare($vehicleHistories);
            $queryForVehicleHistories->execute(array(
                ':imei_number' => $imei_number,
                ':latitude' => $lat,
                ':longitude' => $lon,
                ':vehicle_bearing' => $vehicle_bearing,
                ':speed_limit' => $speed_limit,
                ':last_date' => $lastDate,
                ':last_time' => $lastTime
            ));

            // echo a message to say the UPDATE succeeded
            echo $queryForVehicleHistories->rowCount() . " records created successfully";
            $conn = null;        // Disconnect
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}

//$data = new Connection()

?>