<?php
namespace App\Socket;
use App\Http\Controllers;
use App\Socket;
require("SocketServer.php");
require("Connection.php");
// Include the Class File

abstract class Secure
{
//    protected $ip = '128.199.232.1';
    protected $ip = '192.168.0.109';
    protected $port = '55535';
    public function Begin()
    {
        $server = new SocketServer($this->ip, $this->port); // Create a Server binding to the default IP address (null) and listen to port 31337 for connections
        $server->max_clients = 10; // Allow no more than 100 people to connect at a time

        $server->hook("CONNECT", "handle_connect"); // Run handle_connect everytime someone connects
        $server->hook("INPUT", "handle_input"); // Run handle_input whenever text is sent to the server

        $server->infinite_loop(); // Run Server Code Until Process is terminated.

        /*
         * All hooked functions are sent the parameters $server (The server class), $client (the connection), and $input (anything sent, if anything was sent)
         * You should save the variables $server and $client using an ampersand (&) to make sure they are references to the objects and not duplications.
         */
    }

    function handle_connect(&$server, &$client, $input)
    {
        //convert string to array
        SocketServer::socket_write_smart($client->socket, "(027043620459AP01HSO)", ""); // Outputs 'String? ' without a Line Ending
    }

    public static function inputData(&$server, &$client, $inputString)
    {
        $inputArr = explode('(', $inputString);
        $lengthOfInput = count($inputArr);
        for ($i = 0; $i < $lengthOfInput; $i++) {
//            print_r($inputArr[$i]);

            $input = $inputArr[$i];
            echo $input."\n";
            date_default_timezone_set("Asia/Kolkata");
            if ($input != '') {
                $RepondType = substr($input, 12, 2);
                echo 'found status : ' . $RepondType . '\n';
                if ($RepondType == "BR") {
                    echo "It is a valid string because Response type is  " . $RepondType . "\n";

                    //get Imei
                    $IMEI = substr($input, 0, 12);
                    echo "Identifier : " . $IMEI . "\n";

                    //get date in formet
                    $date = substr($input, 16, 6);
                    $yy = substr($date, 0, 2);
                    $mm = substr($date, 2, 2);
                    $dd = substr($date, 4, 2);
                    $dateInFormat = '20' . $yy . '-' . $mm . '-' . $dd;
                    echo "Date : " . $date . "\n";
                    echo "Date in format: " . $dateInFormat . "\n";

                    //get Latitude
                    $latDegree = substr($input, 23, 2);
                    $latDecimal = substr($input, 25, 7);
                    $accurateDecimal = round($latDecimal / 60, 8);
                    $RemoveZeroBeforeDot = substr($accurateDecimal, 1, 9);
                    $latInDDF = $latDegree . $RemoveZeroBeforeDot;
                    echo "Latitude : " . $latInDDF . "\n";

                    //get Longitude
                    $longDegree = substr($input, 34, 2);
                    $longDecimal = substr($input, 36, 7);
                    $accurateDecimalLng = round($longDecimal / 60, 8);
                    $RemoveZeroBeforeDotLng = substr($accurateDecimalLng, 1, 9);
                    $longInDDF = $longDegree . $RemoveZeroBeforeDotLng;
                    echo "Longitude : " . $longInDDF . "\n";

                    //get speed
                    $speed = substr($input, 44, 5);
                    echo "Speed : " . $speed . "\n";

                    //get time in format
                    $time = substr($input, 49, 6);
                    //set meridian
                    if ($time > 120000) {
                        $meridian = "PM";
                    } else $meridian = "AM";
                    $hh = substr($time, 0, 2);
                    $mi = substr($time, 2, 2);
                    $ss = substr($time, 4, 2);
                    $timeInFormat = $hh . ':' . $mi . ':' . $ss . " " . $meridian;
                    echo "Time : " . $time . "\n";
                    echo "Time in format : " . $timeInFormat . "\n";

                    //get Distance In Km
                    $distance = substr($input, 70, 8);
                    $distanceInKm = round($distance / 0.6214, 3);
                    echo "Distance In Km : " . $distanceInKm . "\n";

                    //create con object of connection class
                    $con = new Connection();
                    //call connect function for
                    $con->connect($IMEI, $speed, $latInDDF, $longInDDF, $distanceInKm, $dateInFormat, $timeInFormat);
                } elseif ($RepondType == "BP") {
                    SocketServer::socket_write_smart($client->socket, "(027043620459AP01HSO)", "\n"); // Outputs 'String? ' without a Line Ending
                    echo 'msg sent to client  : (027043620459AP01HSO) '."\n";
                } else {
                    echo 'Wrong Device trying to connect';
                }
            } else {
                echo "Sorry no data found !" . $input;
            }
        }

    }

    function handle_input(&$server, &$client, $input)
    {
        $trim = trim($input); // Trim the input, Remove Line Endings and Extra Whitespace.

        if (strtolower($trim) == "quit") // User Wants to quit the server
        {
            SocketServer::socket_write_smart($client->socket, "Oh... Goodbye..."); // Give the user a sad goodbye message, meany!
            $server->disconnect($client->server_clients_index); // Disconnect this client.
            return; // Ends the function
        }

        $output = strrev($trim); // Reverse the String

        SocketServer::socket_write_smart($client->socket, $output); // Send the Client back the String
        SocketServer::socket_write_smart($client->socket, "String? ", ""); // Request Another String
    }

    public function geoLocationAlert()
    {
        $geoLocationAlert = new Controllers\GeoFenceController();

        return $geoLocationAlert->startFence();
    }

    public function unbind()
    {
        print_r("trying to unbind socket".$this->server);
        socket_close($this->server);
        print_r("socket unbind successfully");
    }
}

class Index extends Secure
{
    private static $instance = NULL;

    static public function getInstance()
    {
        if (self::$instance === NULL)
            self::$instance = new Index();
        return self::$instance;
    }
}
//
//$instance = Index::getInstance();
//$instance->Begin();
