<?php namespace App;

use Illuminate\Contracts\Auth\Guard;
use App\Repositories\UserRepository as UserRepository;
use Laravel\Socialite\Contracts\Factory as Socialite;

class AuthenticateUserSocial
{

    // use private keyword because variable scope only in this class
    private $users;
    private $socialite;
    private $auth;

//    private $provider;
    public function __construct(UserRepository $users, Socialite $socialite, Guard $auth)
    {
        $this->users = $users;
        $this->socialite = $socialite;
        $this->auth = $auth;
    }

    public function execute($provider, $hasCode, AuthenticateUserListener $listener)
    {
        //get login service provider
//        $this->provider = $provider;
        //check if code not available then getAuthorisationFirst
        if (!$hasCode) return $this->getAuthorisationFirst($provider);

        //create new record if not available
        //findOrCreateNewUser() is available in UserRepository() class
        $user = $this->users->findOrCreateNewUser($this->socialUsers($provider));
        //this->auth is a object of Guard interface (Guard interface have all methods of authenticate a user)
        $this->auth->login($user, true);

        //userHasLogedIn function is in SocialLiteController but it is call via interface AuthenticateUserListener
        return $listener->userHasLogedIn($user);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function getAuthorisationFirst($provider)
    {
        return $this->socialite->driver($provider)->redirect();
    }

    /**
     * @return \Laravel\Socialite\Contracts\User
     */
    private function socialUsers($provider)
    {

        $socialUsers = $this->socialite->driver($provider)->user();
        return $socialUsers;
    }

}