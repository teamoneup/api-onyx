<?php
namespace App\OnyxApi;

use App\User;
use Validator;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ValidationOfOnyxApi extends Controller
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function redirectOnSuccess(Request $request)
    {
        //getting user id from email
        $userId = User::userId($request->get('email'));

        //find user details by user id
        $user = User::find($userId);

        //set user detail and set to true if user email or password is correct
        $this->auth->login($user, true);
        //set User Authentication status is true
        return $this->setStatusCode(200)
            ->respondWithResponce('Welcome you', 'Success');
//            return redirect('/');
    }

    /**
     * @param Request $request
     * @param $validator
     * @return mixed
     */
    public function respondWithValidation(Request $request, $validator)
    {
        $collectionOfValidations = $validator->getMessageBag();

        if ($collectionOfValidations->get('password') != null) {
            return $this->setStatusCode(400)
                ->respondWithResponce($collectionOfValidations->get('password'), 'Sorry');
        } elseif ($collectionOfValidations->get('email') != null) {
            return $this->setStatusCode(400)
                ->respondWithResponce($collectionOfValidations->get('email'), 'Sorry');
        } else {
            $this->create($request->all());
            return $this->setStatusCode(200)
                ->respondWithResponce('You are registered successfully ', 'Thank you');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data,$validationOn)
    {
        return Validator::make($data, $validationOn);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmed' => true,
        ]);
//        return $this->setStatusCode(200)
//            ->respondWithResponce('Thank you for using our services', $data);

    }
}