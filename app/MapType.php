<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapType extends Model
{
    protected $fillable =
        [
            'user_id',
            'map_type_id',
        ];

    public static function changeMapType($maptypeId, $userId)
    {
        return static::where('user_id', '=', $userId)
            ->update([
                'map_type_id' => $maptypeId,
            ]);
    }
    public static function updateMapType($maptypeId, $userId)
    {
        return static::create([
            'user_id' => $userId,
            'map_type_id' => $maptypeId,]);
    }
    public static function newRquestForMap($userId)
    {
        return static::where('user_id', '=',$userId );
    }
}
