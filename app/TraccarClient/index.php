<?php

namespace App\TraccarClient;


use App\Http\Controllers;

use App\TraccarClient;

use DateTime;

use DateTimeZone;


require("SocketServer.php");

require("Connection.php");

// Include the Class File

//$server = new SocketServer("103.253.145.137", 55335); // Create a Server binding to the default IP address (null) and listen to port 31337 for connections

abstract class Secure

{

    public function Begin()
    {
        $ip = '103.253.145.137';

        $port = '33335';

        $server = new SocketServer($ip, $port); // Create a Server binding to the default IP address (null) and listen to port 31337 for connections


        $server->max_clients = 10; // Allow no more than 10 people to connect at a time

        $server->hook("CONNECT", "handle_connect"); // Run handle_connect everytime someone connects

        $server->hook("INPUT", "handle_input"); // Run handle_input whenever text is sent to the server


        $server->infinite_loop(); // Run Server Code Until Process is terminated.


        /*

         * All hooked functions are sent the parameters $server (The server class), $client (the connection), and $input (anything sent, if anything was sent)

         * You should save the variables $server and $client using an ampersand (&) to make sure they are references to the objects and not duplications.

         */

    }


    public static function inputData($input)

    {

        date_default_timezone_set("Asia/Kolkata");

        if ($input != '') {

            $socketInformation = explode('&', $input);

            $imei_number = explode('id=', $socketInformation[0]);// $imei[1]

            $timestamp = explode('timestamp=', $socketInformation[1]);// $timestamp[1]

            $lat = explode('lat=', $socketInformation[2]);// $lat[1]

            $lon = explode('lon=', $socketInformation[3]);// $lon[1]

            $speed_limit = explode('speed=', $socketInformation[4]);// $speed[1]

            $vehicle_bearing = explode('bearing=', $socketInformation[5]);// $bearing[1]

//            $altitude = explode('altitude=', $socketInformation[6]);// $altitude[1]

//            $batt = explode('batt=', $socketInformation[7]);// $batt[1]

            $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));

            $CurrentDate = $date->format('Y-m-d h:i A');

            echo 'imei' . $imei_number[1] . "\n";

            echo 'lat' . $lat[1] . "\n";

            echo 'long' . $lon[1] . "\n";

            echo 'bearing' . $vehicle_bearing[1] . "\n";

            $explodedDeviceTimestamp = explode(' ', $CurrentDate);

            echo 'date' . $explodedDeviceTimestamp[0] . "\n";

            echo 'Time' . $explodedDeviceTimestamp[1] . $explodedDeviceTimestamp[2] . "\n";

            $time = $explodedDeviceTimestamp[1] . $explodedDeviceTimestamp[2];

            $con = new Connection();

            $con->connect($imei_number[1], $speed_limit[1], $lat[1], $lon[1], $vehicle_bearing[1], $explodedDeviceTimestamp[0], $time);

        }

        else

            print_r('Data not available', $input."\n");


    }


    function handle_input(&$server, &$client, $input)

    {

        $trim = trim($input); // Trim the input, Remove Line Endings and Extra Whitespace.


        if (strtolower($trim) == "quit") // User Wants to quit the server

        {
            SocketServer::socket_write_smart($client->socket, "Oh... Goodbye..."); // Give the user a sad goodbye message, meany!

            $server->disconnect($client->server_clients_index); // Disconnect this client.

            return; // Ends the function
        }


        $output = strrev($trim); // Reverse the String


        SocketServer::socket_write_smart($client->socket, $output); // Send the Client back the String

        SocketServer::socket_write_smart($client->socket, "String? ", ""); // Request Another String

    }

    public function geoLocationAlert()

    {

        $geoLocationAlert = new Controllers\GeoFenceController();

        return $geoLocationAlert->startFence();

    }

}


class Index extends Secure

{

    private static $instance = NULL;

    static public function getInstance()

    {

        if (self::$instance === NULL)

            self::$instance = new Index();

        return self::$instance;

    }

}

//
//
//$instance = Index::getInstance();
//
//$instance->Begin();
