<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;


class Vehicle extends Model
{
    protected $table = 'vehicles';
    protected $primaryKey = 'vehicle_id';
    protected $fillable =
        [
            'user_id',
            'vehicle_name',
            'registration_number',
            'imei_number',
            'sim_number',
            'speed_limit',
            'vehicle_status',
            'current_lat',
            'current_long',
            'speed_limit'
        ];

    /**
     * @param array $request
     * @return string
     */
    public static function chackArrayKeyExists(array $request , $key, $default = null)
    {
        return array_key_exists($key, $request) ? $request[$key] : $default;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function geoFence()
    {
        return $this->hasMany('App\GeoFence');
    }

    public function geo()
    {
        return $this->hasMany('App\Vehicle');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicleHistory()
    {
        return $this->hasMany('App\VehicleHistory');
    }

    /**
     * @param $userId
     * @return mixed
     */
    public static function userId($userId)
    {
       return static::where('user_id', '=',$userId );
    }

    /**
     * @param $selectedMapIds
     * @param $userId
     * @return mixed
     */
    public static function selectedVehicleOnMap($selectedMapIds,$userId)
    {
        return static::where('user_id', '=', $userId)
            ->whereIn('vehicle_id',$selectedMapIds);
    }

    /**
     * @param $fieldName
     * @param $valueToFind
     * @return mixed
     */
    public static function findRow($fieldName,$valueToFind)
    {
        return static::where($fieldName,'=',$valueToFind);
    }

    /**
     * @param $imei_number
     * @param $lat
     * @param $lon
     * @return mixed
     */
    public static function updateVehicleByGps($imei_number, $lat, $lon)
    {
        return static::where('imei_number', '=', $imei_number)
            ->update([
                'current_lat' => $lat,
                'current_long' => $lon
            ]);
    }

    /**
     * @param $WhatIWant
     * @param $fieldName
     * @param $result
     * @return mixed
     */
    public static function getVehicleDetail($WhatIWant,$fieldName,$result)
    {
        return static::where($fieldName, '=', $WhatIWant)
            ->value($result);
    }

    /**
     * @param $vehicleId
     * @return mixed
     */
    public static function VehicleListDetails($vehicleId)
    {
        return static::where('vehicle_id', '=', $vehicleId)->value('vehicle_name');
    }


    /**
     * @param array $request
     * @param $userId
     * @return static
     */
    public static function vehicleAddToDb(array $request,$userId)
    {
        list($current_long, $current_lat, $sim_number, $registration_number, $speed_limit) = static::findExistanceOfData($request);

        return static::create([
            'user_id' => $userId,
            'vehicle_name' => $request['vehicle_name'], //mandatory
            'registration_number' => $registration_number,
            'imei_number' => $request['imei_number'], //mandatory
            'sim_number' => $sim_number,
            'speed_limit' => $speed_limit,
            'vehicle_status' => 'Active',
            'current_lat' => $current_lat,
            'current_long' => $current_long ,
        ]);
    }

    /**
     * @param array $request
     * @return array
     */
    public static function findExistanceOfData(array $request)
    {
        $current_long = self::chackArrayKeyExists($request,'current_long','73.043494');
        $current_lat = self::chackArrayKeyExists($request,'current_lat','28.043494');
        $sim_number = self::chackArrayKeyExists($request,'sim_number');
        $registration_number = self::chackArrayKeyExists($request,'registration_number');
        $speed_limit = self::chackArrayKeyExists($request,'speed_limit');

        return array($current_long, $current_lat, $sim_number, $registration_number, $speed_limit);
    }

    public static function getVehicleIdThroughImeiNo($imei_number, $userId)
    {
        return static::where('imei_number',$imei_number)
            ->where('user_id',$userId)
            ->value('vehicle_id');
    }

    public static function getVehicleIdThroughImeiNoInArray($imei_number)
    {
        return static::whereIn('imei_number', $imei_number);
    }

}
