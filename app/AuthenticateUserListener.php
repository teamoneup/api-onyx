<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 12/24/2015
 * Time: 3:48 PM
 */
namespace App;

interface AuthenticateUserListener
{
    public function userHasLogedIn($user);
}