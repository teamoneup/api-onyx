<?php

namespace App;

//use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

class GeoFence extends Model
{
    protected $table = 'geo_fences';
    protected $fillable =
        [
            'vehicle_id',
            'landmark_name',
            'geo_fences_coordinates',
            'date_from',
            'time_from',
            'time_to',
            'date_to',
            'days',
            'status',
        ];

    /**
     * @param array $request
     * @return string
     */
    public static function findDaysAvailableOrNot(array $request)
    {
        $alldays = '';
        $days = array_key_exists('days', $request) ? $request['days'] : '';

        if ($days != null) {
            $alldays = implode(',', $request['days']);
            return $alldays;
        }
        return $alldays;
    }


    public function vehicleFence()
    {
        return $this->hasMany('App\Vehicle');
    }


    public static function storeGeoFence(array $request, $vehicle_id)
    {

        list($date_from, $date_to, $time_from, $time_to, $status, $alldays, $geoFencesCoordinates, $landmark_name)
            = self::checkGeoFenceFieldExistance($request);

        return static:: create([
            'vehicle_id' => $vehicle_id,
            'landmark_name' => $landmark_name, //mandatory
            'geo_fences_coordinates' => $geoFencesCoordinates,
            'date_from' => $date_from,
            'time_from' => $time_from,
            'date_to' => $date_to,
            'time_to' => $time_to,
            'status' => $status,
            'days' => $alldays
        ]);
    }

    public static function updateGeoFence(array $request, $updateGeoFenceid, $vehicleIdFromApi = null)
    {


        list($date_from, $date_to, $time_from, $time_to, $status, $alldays, $geoFencesCoordinates, $landmark_name, $vehicleId) = self::checkGeoFenceFieldExistance($request);

        if ($vehicleIdFromApi != null) {
            $vehicleIdUpdate = $vehicleIdFromApi;
        }
        else {
            $vehicleIdUpdate = $vehicleId;
        }


        if ($geoFencesCoordinates == "") {
            $geoFencesCoordinates = static::where('id', '=', $updateGeoFenceid)
                                            ->value('geo_fences_coordinates');

        }
        return static::where('id', '=', $updateGeoFenceid)
            ->update([
                'vehicle_id' => $vehicleIdUpdate,
                'landmark_name' => $landmark_name, //mandatory
                'geo_fences_coordinates' => $geoFencesCoordinates,
                'date_from' => $date_from,
                'time_from' => $time_from,
                'date_to' => $date_to,
                'time_to' => $time_to,
                'status' => $status,
                'days' => $alldays
            ]);
    }


    public function vehicleGeoFence()
    {
        return $this->belongsTo('App\Vehicle');
    }

    public static function selectDateTime($geofenceeditid, $selectedItem)
    {
//        dd($selectedItem);

        return static::where('vehicle_id', '=', $geofenceeditid)
            ->value($selectedItem);

    }
    public static function selectedDays($geofenceeditid)
    {
        return static::where('vehicle_id', '=', $geofenceeditid)->value('days');
    }
    public static function selectedGeoFanceOnHome(Request $request)
    {
        return static::where('vehicle_id', '=', $request->selectedId)
            ->get([
                'id',
                'landmark_name',
                'days',
                'time_to',
                'time_from',
                'status',
                'vehicle_id'
            ]);
    }

    /**
     * It will make the other geofence inactive when we make "active" status to a specific vehicle
     * @param $vehicleId
     */
    public static function checkGeofenceStatus($vehicleId)
    {
        static::where('vehicle_id', '=', $vehicleId)
            ->update(array('status' => 'inactive'));
    }

    public static function geoFenceCoordinatesInProperMannner($selectedGeoIdCoordinates)
    {
        //(28.565225490654658, 73.6072998214513)(28.391400375817753, 73.9368896652013)
        $geoFencesCoordinatesPS = str_replace('(', '', $selectedGeoIdCoordinates);
        $geoFencesCoordinatesPE = str_replace(')', '|', $geoFencesCoordinatesPS);
        $geoFencesCoordinatesPEFinal = chop($geoFencesCoordinatesPE, '|');
        return $geoFencesCoordinatesPEFinal;
    }

    /**
     * @param array $request
     * @return array
     */
    public static function checkGeoFenceFieldExistance(array $request)
    {
        $geo_fences_coordinates = array_key_exists('geo_fences_coordinates', $request) ? $request['geo_fences_coordinates'] : '';

        $date_from = array_key_exists('date_from', $request) ? $request['date_from'] : '';
        $date_to = array_key_exists('date_to', $request) ? $request['date_to'] : '';

        $time_from = array_key_exists('time_from', $request) ? $request['time_from'] : '';
        $time_to = array_key_exists('time_to', $request) ? $request['time_to'] : '';

        $status = array_key_exists('status', $request) ? $request['status'] : 'Active';
        $landmark_name = array_key_exists('landmark_name', $request) ? $request['landmark_name'] : '';

        $vehicleId = array_key_exists('vehicle_id', $request) ? $request['vehicle_id'] : 'Active';
        $alldays = self::findDaysAvailableOrNot($request);

        $geoFencesCoordinates = static::geoFenceCoordinatesInProperMannner($geo_fences_coordinates);
        return array($date_from, $date_to, $time_from, $time_to, $status, $alldays, $geoFencesCoordinates, $landmark_name, $vehicleId);
    }

    public static function getGeoFenceIdByVehicleIdAndLandmarkName($vehicleId,$landmarkName)
    {
        return static::where('vehicle_id','=',$vehicleId)
                     ->where('landmark_name','=',$landmarkName)
                     ->value('id');
    }
}
