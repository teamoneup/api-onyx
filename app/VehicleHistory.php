<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleHistory extends Model
{
    protected $table = 'vehicle_histories';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
    public static function findRow($fieldName,$valueToFind)
    {
        return static::where($fieldName,'=',$valueToFind);
    }

    /**
     * @param $IMEI
     * @param array $request
     * @return mixed
     */
    public static function findSelectedHistory($IMEI, array $request)
    {
        list($dateStart, $dateEnd, $timeStart, $timeEnd) = self::findExistanceOfFields($request);

        if ($dateStart != null && $timeStart != null) {
            return static::whereBetween('last_date', [$dateStart, $dateEnd])
                ->whereBetween('last_time', [$timeStart,$timeEnd])
                ->where('imei_number', '=', $IMEI)
                ->orderBy('last_date','desc')
//                ->orderBy('last_time','desc')
                ->get();
        }
        else {
            return static::where('imei_number', '=', $IMEI)
                ->orderBy('last_date','desc')
                ->orderBy('last_time','desc')
                ->get();
        }
    }

    /**
     * @param $imei_number
     * @param $speed_limit
     * @param $lat
     * @param $lon
     * @param $vehicle_bearing
     * @param $lastDate
     * @param $lastTime
     * @return static
     */
    public static function CreateHistoryVehicleByGps($imei_number, $speed_limit, $lat, $lon, $vehicle_bearing, $lastDate, $lastTime)
    {
        return static::create([
            'imei_number' => $imei_number,
            'latitude' => $lat,
            'longitude' => $lon,
            'vehicle_bearing' => $vehicle_bearing,
            'speed_limit' => $speed_limit,
            'last_date' => $lastDate,
            'last_time' => $lastTime
        ]);
    }

    /**
     * @param $imei_number
     * @return mixed
     */
    public static function counterForLocationUpdate($imei_number)
    {
        return static::where('imei_number', '=',$imei_number );
    }
    /**
     * @param array $request
     * @return array
     */
    public static function findExistanceOfFields(array $request)
    {
        $dateStart = array_key_exists('date_start', $request) ? $request['date_start'] : null;
        $dateEnd = array_key_exists('date_end', $request) ? $request['date_end'] : '';

        $timeStart = array_key_exists('time_start', $request) ? $request['time_start'] : null;
        $timeEnd = array_key_exists('time_end', $request) ? $request['time_end'] : '';
        return array($dateStart, $dateEnd, $timeStart, $timeEnd);
    }

}
