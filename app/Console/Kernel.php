<?php

namespace App\Console;

use App\Socket\Index;
use App\Socket\SocketServer;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\LocationUpdater::class,
        \App\Console\Commands\SocketUnbind::class,

        \App\Console\Commands\TraccarClient::class,

        \App\Console\Commands\LogDemo::class,
        \App\Console\Commands\GeoFenceAlert::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //unbind socket through this command
        $schedule->command('socket:stop');

        //command for updating history of vehicle by Traccar
        $schedule->command('track:run')->everyMinute();

           //command for updating history of vehicle by GPS device
        $schedule->command('update:location')->everyMinute();
        //command for geo alert
        $schedule->command('geo:alert')->everyMinute();
        $schedule->command('inspire')->hourly();
        $schedule->command('log:demo')->everyMinute();
    }
}
