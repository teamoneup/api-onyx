<?php

namespace App\Console\Commands;
use Log;
use Illuminate\Console\Command;
use App\Socket\Index;

class LogDemo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:demo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update history table ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('testing log file: ');
            $instance = Index::getInstance();
            $instance->Begin();


    }
}
