<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Socket\Index;

class GeoFenceAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geo:alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'commend For sending "geo fence" alert ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $instance = Index::getInstance();
        $instance->geoLocationAlert();

    }
}
