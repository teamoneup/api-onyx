<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TraccarClient\Index as tc;


class TraccarClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'track:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get data from traccar Client ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $instance =  tc::getInstance();
        $instance->Begin();
    }
}
