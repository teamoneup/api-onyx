<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Socket\SocketInstance;


class LocationUpdater extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Location updater of vehicle and populate history new';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $instance = SocketInstance::getInstance();
        $instance->startBinding();
    }
}
