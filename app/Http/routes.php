<?php

/*
|
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('home', 'HomeController@home');
Route::get('/', 'HomeController@home');
//Route::post('/','HomeController@show');

//Password reset Start
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
//Password reset End

Route::any('history', 'HomeController@history');
//History Root
Route::any('auth/login-credential/{password}/{email}', 'UserController@checkLoginCredentialByJS');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'UserController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::post('auth/register', 'UserController@postRegister');

// Redirect to github to authenticate
Route::any('login/{provider}', 'SocialLiteController@login');

Route::get('managevehicle', 'VehicleController@manageVehicle');
Route::get('addvehicle', 'VehicleController@addVehicle');
Route::post('managevehicle', 'VehicleController@saveVehicle');

Route::get('managevehicle/{id}', 'VehicleController@editVehicle');
Route::post('managevehicle/{id}', 'VehicleController@updateVehicle');
Route::get('deletevehicle/{id}', 'VehicleController@deleteVehicle');

//History
Route::any('viewhistory', 'VehicleController@viewHistory');
//Report
Route::get('managereport', 'VehicleController@manageReport');

Route::get('addgeofence', 'VehicleController@addGeoFence');
Route::post('savegeofence', 'VehicleController@saveGeoFence');
Route::get('editgeofence/{geofenceid}', 'VehicleController@editGeoFence');
Route::post('editgeofence/{geofenceid}', 'VehicleController@updateGeoFence');
Route::get('deletegeofence/{delgeofence}', 'VehicleController@deleteGeoFence');

Route::get('managegeofence', 'VehicleController@managegeofence');
Route::post('managegeofence', 'VehicleController@selectedGeoFence');
//Route::post('showgeofenceonhome/{selectedGeoId}','VehicleController@selectedGeoFence');

//For setting Page
Route::get('managesetting', 'HomeController@manageSetting');
Route::post('managesetting', 'HomeController@storeSetting');

//Route::get('socket','SocketController@connectTO');

Route::any('maps', 'HomeController@maps');

Route::get('startfence', 'GeoFenceController@startFence');

//Route::get('verifyMail/{email}','HomeController@checkYourMail');
Route::get('register/verify/{confirmation_code}', 'Auth\AuthController@confirmedMail');

//set map type id
Route::post('maptype/{maptypeId}', 'HomeController@setMapType');

//set notification to viewed
Route::get('updatenotificationtoviewed', 'HomeController@updateNotificationToViewed');

//show developer tools for api
Route::get('developerApiTool', function()
{
    return view('developer.developerTool');
});


//Routes for Rest full api

Route::group(['prefix' => 'api/onyx'], function () {

    //authentication
    Route::post('register','APIController\ApiAuthController@postRegister');
    Route::post('login','APIController\ApiAuthController@postLogin');
    Route::post('logout','APIController\ApiAuthController@postLogout');

    //manage vehicles
    Route::resource('vehicle','APIController\VehicleController');

    //manage geo fences
    Route::resource('geofence','APIController\GeoFenceController');

    //manage history
    Route::post('history','APIController\HistoryController@ViewHistoryThroughApi');

//    //manage user profile setting
//    Route::post('managesetting', 'VehicleController@storeSetting');

});

