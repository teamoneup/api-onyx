<?php
namespace App\Http\Utilities;

class Days
{
    protected static $days =
        [
            "Monday" => "mon",
            "Tuesday" => "tue",
            "Wednesday" => "wed",
            "Thursday" => "thu",
            "Friday" => "fri",
            "Saturday" => "sat",
            "Sunday" => "sun"
        ];

    public static function day()
    {
        return array_flip(static::$days);
    }

}