<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\GeoFence;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use App\Socket\Index;
use App\User;

class GeoFenceController extends Controller
{
    var $pointOnVertex = true; // Check if the point sits exactly on one of the vertices

    /**
     * set maximum execution time and get all activated geofences, all users and all vehicles
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//
//    }
    public function startFence()
    {
        set_time_limit(0);

        $allVehicle = Vehicle::all();

        $allUsers = User::all();

        $allActiveGeoFences = GeoFence::where('status','=','active')->get();

        $this->checkFence($allVehicle,$allActiveGeoFences,$allUsers);
    }


    /**
     * *Start checking if vehicle is in active geofence or not,
     * @param $allVehicle
     * @param $allActiveGeoFences
     * @param $allUsers
     */
    public function checkFence($allVehicle,$allActiveGeoFences,$allUsers)
    {
        foreach($allActiveGeoFences as $geoFence)
        {
            $geofenceName = $geoFence->landmark_name;

            //loop for get geoFence's vehicle info
            foreach($allVehicle as $vehicle)
            {
                if($vehicle->vehicle_id == $geoFence->vehicle_id)
                {
                    $vehicleLastLat = $vehicle->current_lat;

                    $vehicleLastLong = $vehicle->current_long;

                    $vehicleName = $vehicle->vehicle_name;

                    $point = $vehicleLastLat." ".$vehicleLastLong;//make last lat long of vehicle to check

                    //loop for get geofence's user info
                    foreach($allUsers as $user)
                    {
                        if($user->id == $vehicle->user_id)
                        {
                            $userEmail = $user->email;
//                        $userName = $user->name;
                        }
                    }
                }
            }
            if($geoFence->geo_fences_coordinates != "") {

                $coordinatesToFix = str_replace(", ", " ", $geoFence->geo_fences_coordinates);

                $coordinatesFixed = str_replace("|", ',', $coordinatesToFix);

                $polygon = explode(',', $coordinatesFixed);


                //check if geofence has complete desire date or not
                if (($geoFence->date_from != "") && ($geoFence->date_to != "")) {
                    $resultDate = $this->checkDateInRange($geoFence->date_from, $geoFence->date_to);
                }
                //check if geofence has complete desire time or not
                if (($geoFence->time_from != "") && ($geoFence->time_to != "")) {
                    $resultTime = $this->checkTimeInRange($geoFence->time_from, $geoFence->time_to);
                }
                //check if geofence has desire days or not
                if ($geoFence->days != "") {
                    $resultDays = $this->checkDaysInRange($geoFence->days);
                }

                //CASE 1 :check geofence and send alert when geofence don't have all 3 things- time, date and day
                if (!isset($resultDate) && !isset($resultTime) && !isset($resultDays)) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 2 : check geofence and send alert when all 3 things- time, date and day fullfill current time,day,date
                elseif ((isset($resultDate) && ($resultDate === true)) && (isset($resultTime) && ($resultTime === true)) && (isset($resultDays) && ($resultDays === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 3 : check geofence and send alert when time, date is null but day fullfill current day
                elseif ((!isset($resultDate) && !isset($resultTime)) && (isset($resultDays) && ($resultDays === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 4 : check geofence and send alert when time, days is null but date fullfill current date
                elseif ((!isset($resultDays) && !isset($resultTime)) && (isset($resultDate) && ($resultDate === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 5 : check geofence and send alert when date, days is null but time fullfill current time
                elseif ((!isset($resultDate) && !isset($resultDays)) && (isset($resultTime) && ($resultTime === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 6 : check geofence and send alert when days is null but time, date fullfill current time, date
                elseif (!isset($resultDays) && (isset($resultDate) && ($resultDate === true)) && (isset($resultTime) && ($resultTime === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 7 : check geofence and send alert when date is null but time, days fullfill current time, days
                elseif (!isset($resultDate) && (isset($resultDays) && ($resultDays === true)) && (isset($resultTime) && ($resultTime === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                } //CASE 8 : check geofence and send alert when time is null but days, date fullfill current days, date
                elseif (!isset($resultTime) && (isset($resultDate) && ($resultDate === true)) && (isset($resultDays) && ($resultDays === true))) {
                    $this->checkFenceAndSendMail($point, $polygon, $userEmail, $vehicleName, $geofenceName);
                }
                unset($resultDate);
                unset($resultDays);
                unset($resultTime);
            }
        }//geofence foreach end

    }

    /**
     * check if vehicle outside from geofence then send mail
     * @param $point
     * @param $polygon
     * @param $userEmail
     * @param $vehicleName
     * @param $geofenceName
     */
    public function checkFenceAndSendMail($point, $polygon,$userEmail,$vehicleName,$geofenceName)
    {
        $vehicleCurrentLocation = $this->pointInPolygon($point, $polygon);
        if($vehicleCurrentLocation == "outside")
        {
            $this->sendEmail($userEmail,$vehicleName,$geofenceName);
        }
    }

    /**
     * check either point(vehicle) is in polygon(geofence) or not
     * @param $point
     * @param $polygon
     * @param bool|true $pointOnVertex
     * @return string
     */
    public function pointInPolygon($point, $polygon, $pointOnVertex = true)
    {
        $this->pointOnVertex = $pointOnVertex;

        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array();
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex);
        }

        // Check if the point sits exactly on a vertex
//        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true)
//        {
//            return "vertex";
//        }

        // Check if the point is inside the polygon or on the boundary
        $intersections = 0;
        $vertices_count = count($vertices);

        for ($i = 1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i - 1];
            $vertex2 = $vertices[$i];

//            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x']))
//            { // Check if point is on an horizontal polygon boundary
//                return "boundary";
//            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) {
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x'];
//                if ($xinters == $point['x'])
//                { // Check if point is on the polygon boundary (other than horizontal)
//                    return "boundary";
//                }
//                dd($xinters);
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++;
                }
            }
        }
        // If the number of edges we passed through is odd, then it's in the polygon.
        if ($intersections != 0) {
            return "inside";
        } else {
            return "outside";
        }
    }

    public function pointOnVertex($point, $vertices)
    {
        foreach ($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
    }

    public function pointStringToCoordinates($pointString)
    {
        $coordinates = explode(" ", $pointString);
        return array("x" => $coordinates[0], "y" => $coordinates[1]);
    }

    /**
     * @param $start_date
     * @param $end_date
     * @return bool
     */
    public function checkDateInRange($start_date, $end_date)
    {
        $todayDate = date('d-m-Y');

        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $today_ts = strtotime($todayDate);

        // Check that today date is between start & end
        return (($today_ts >= $start_ts) && ($today_ts <= $end_ts));
    }

    /**
     * @param $start_time
     * @param $end_time
     * @return bool
     */
    public function checkTimeInRange($start_time, $end_time)
    {
        $todayTime = date ('h:i:s A');

        // Convert to timestamp
        $start_ts = strtotime($start_time);
        $end_ts = strtotime($end_time);
        $today_ts = strtotime($todayTime);

        // Check that today date is between start & end
        return (($today_ts >= $start_ts) && ($today_ts <= $end_ts));
    }

    /**
     * @param $days
     * @return bool
     */
    public function checkDaysInRange($days)
    {
        $today = strtolower(date('D'));

        if (strpos($days,$today)!== false)
        {
            return true;
        }
        else
            return false;
    }

    /**
     * @param $emailTo
     * @param $vehicleName
     * @param $geofenceName
     */
    public function sendEmail($emailTo,$vehicleName,$geofenceName)
    {

        $emailContent = array(
            'emailTo'=>$emailTo,
            'vehicleName'=>$vehicleName,
            'geoFenceName'=>$geofenceName
        );
        // Sending mail(alert) to Vehicle owner
        Mail::send('mails.alertMail', $emailContent, function($message) use($emailContent)
        {
            $message->to($emailContent['emailTo'] )->subject('Alert from ONYX GPS about: '.$emailContent['vehicleName'] );
        });
    }

}
