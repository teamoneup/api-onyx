<?php
namespace App\Http\Controllers;

use Auth;
use Image;
use App\User;
use App\MapType;
use App\Vehicle;
use App\GeoFence;
use App\VehicleHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\SettingInfoRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'confirmedMail']);

        parent::__construct();
                $this->middleware('auth');

    }

    public function maps(Request $request)
    {
        if ($request->has('selectedMap') == false) {

            $vehicleDetail = Vehicle::userId($this->userId)->get();

        } else {
            $selectedMapIds = $request->get('selectedMap');

            $vehicleDetail = Vehicle::selectedVehicleOnMap($selectedMapIds, $this->userId)->get();
        }

        $mapTypeId = MapType::newRquestForMap($this->userId)->value('map_type_id');

        $histories = 'null';

        return view('map.maps', compact('vehicleDetail', 'histories', 'mapTypeId'));
    }

    public function home()
    {
        return view('home');
    }

    public function history(Request $request)
    {
        $imei = Vehicle::findRow('vehicle_id', $request->get('vehicle_id'))->value('imei_number');

        $vehicleName = Vehicle::findRow('vehicle_id', $request->get('vehicle_id'))->value('vehicle_name');

        $mapTypeId = MapType::newRquestForMap($this->userId)->value('map_type_id');

        $histories = VehicleHistory::findSelectedHistory($imei, $request->all());

        $vehicleList = Vehicle::userId($this->userId)->lists('vehicle_Name', 'vehicle_id');

        $vehicleDetail = Vehicle::userId($this->userId)->get();

        return view('map.maps', compact('histories', 'vehicleName', 'vehicleId', 'vehicleList', 'vehicleDetail', 'mapTypeId'));
    }

    public function setMapType($maptypeId)
    {
        MapType::changeMapType($maptypeId, $this->userId);
    }

    public function updateNotificationToViewed()
    {
        User::updateNotificationCountToZero($this->userId);
    }
    /**
     * Manage Setting of user's profie.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageSetting()
    {
        $VehicleList = Vehicle::where('user_id', '=', $this->userId)->orderBy('vehicle_name', 'asc')->paginate(10);

        $profilePicture = User::userProfilePicture($this->userId, 'user_profile_image');

        $member_since = auth::user()->created_at;

        $member_since = explode(' ', $member_since);

        return view('setting.manageSetting', compact('member_since', 'VehicleList', 'profilePicture'));
    }

    /**
     * Store a user profile info in database.
     * @param SettingInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSetting(SettingInfoRequest $request)
    {
        if ($request->hasFile('image')) {

            $file = Input::file('image');

            $name = $file->getClientOriginalName();
//            $getClientOriginalExtension ='_top.'.$file->getClientOriginalExtension();

            $file->move('images/userProfile', $name);

            //image resize
            Image::make('images/userProfile/' . $name)
                ->resize(100,100)
                ->save('images/userProfile/'. 'top_' . $name);

            $user_profile_image_Th = 'images/userProfile/'. 'top_' . $name;
            $user_profile_image = 'images/userProfile/' . $name;


        }

        else
        {
            $user_profile_image = User::userProfilePicture($this->userId , 'user_profile_image');
            $user_profile_image_Th = User::userProfilePicture($this->userId , 'user_thumbnails');
        }

        User::StoreUserProfile($request, $user_profile_image,$user_profile_image_Th, $this->userId);

        flash()->success('Success!', 'Your Information updated successfully!');

        return redirect()->back();
    }

}
