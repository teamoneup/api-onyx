<?php

namespace App\Http\Controllers;

use App\MapType;
use Auth;
use Session;
use App\User;
use Validator;
use App\Notification;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Auth\Authenticatable;


class UserController extends Controller
{
    use Authenticatable;

    protected $auth;

    public function __construct(Guard $auth)
    {
        parent::__construct();
        $this->auth = $auth;
    }

    public function postLogin(Request $request)
    {
//        Session::forget('login_credential');
//        $userType = User::where('email', $request->email)->value('user_type');

//        if ($userType == null) {
//
//            return redirect()->back()->withErrors(['login_credential' => 'Sorry! you are not registered with Health-Care ']);
//
//        }
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

//        if ($userType == 'admin' || $userType == 'auditor') {

        if (Auth::attempt($this->getCredentials($request))) {
            return redirect()->intended('/');
//                return redirect('admin');

        } else {
//                Session::put(['login_credential' => 'Sorry! Entered credential dose not matched with our record']);
            return redirect()->back()->withErrors(['login_credential' => 'Sorry! Entered credential dose not matched with our record']);
//                return redirect('/');

        }


//        } else {
//            {
//
//                return redirect()->back()->withErrors(['login_credential' => 'Please check your mail you are not verify yet']);
//
//            }

//        }
    }

    /**
     * @param Request $request
     * @return array
     */

    public function checkLoginCredentialByJS($password, $email)
    {
        $authStatus = Auth::attempt(array('password' => $password, 'email' => $email));

        if (!$authStatus) {

            return ['validation' => 'Invalid Email or Password'];

//                ->respondWithResponce('Invalid Email or Password', 'Error');
        } else {
            //getting user id from email
            $userId = User::userId($email);

            //find user details by user id
            $user = User::find($userId);

            //set user detail and set to true if user email or password is correct
            $this->auth->login($user, true);
            //set User Authentication status is true
            return ['validation' => ''];
//            return redirect('/');
        }
    }

    public function getCredentials(Request $request)
    {
        return [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users',
            'name' => 'required',
            'passwords' => 'required|min:6',

        ]);

        $user = User::saveUserRegistration($request);

        if($user->id != null)
        {
//           update notification counter in user table
            User::updateNotificationCount($user->id);

//          set map type on register
            MapType::updateMapType('SATELLITE',$user->id);

            Notification::updateUserIdInNotificationTable($user->id, 'You registered successfully!.', 'userRegistration');

//            Session::flash('flash_success', 'You registered successfully! Please Check your email');
            Session::flash('flash_success', 'You registered successfully');

            return redirect('/');
        }

        return redirect()->back();


    }
}
