<?php

namespace App\Http\Controllers;

use App\AuthenticateUserListener;
use Illuminate\Http\Request;
//use App\Http\Requests;
use Auth;
use App\AuthenticateUserSocial;

class SocialLiteController extends Controller implements AuthenticateUserListener
{
    private $hasCode = "code";
    /**
     * AuthenticateUserListener is a interface which is use for use userHasLogedIn() function outside of this class
     * @param AuthenticateUserSocial $authenticateUser
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * AuthenticateUserSocial is a class which is handel execution of login process
     * $request->has ('code') mean redirect path has ?code variable or not
     * $this variable is attach all implements and class object in it
     */
    protected $provider;
    public function login($providers, AuthenticateUserSocial $authenticateUser, Request $request)
    {

        if ($providers == "gitlogin") {
            $this->provider = 'github';
        }
        if ($providers == "googlelogin") {
            $this->provider = 'google';
        }
        if ($providers == "facebooklogin") {
            $this->provider = 'facebook';
        }
        if ($providers == "twitterlogin") {
            $this->hasCode = "oauth_token";
            $this->provider = 'twitter';
        }

        //AuthenticateUser
        return $authenticateUser->execute($this->provider, $request->has($this->hasCode), $this);
    }

    /**
     * @param $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * redirect authenticated user
     */
    public function userHasLogedIn($user)
    {
        return redirect('/');
    }
}
