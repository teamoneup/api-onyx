<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Vehicle;
use App\GeoFence;
Use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;

abstract class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $userId;

    public function __construct()
    {

        if(!Auth::guest()) {
            $this->userId = Auth::user()->id;
        }

    }
    public function findFromObject($thingToSearch,$object)
    {
        $string = $object->implode($thingToSearch, ',');
        //convert string to array
        return explode(",",$string);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getAllGeoFences($userId)
    {
        $vehicleId = $this->getVehicleId($userId)->get(['vehicle_id']);
        $vehicleIdList = $this->findFromObject('vehicle_id', $vehicleId);
        $allGeoFence = GeoFence::whereIn('vehicle_id', $vehicleIdList)->get();
        return $allGeoFence;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function selectedGeoFences($selectedVehicleGeofence)
    {
        $allGeoFence = GeoFence::whereIn('vehicle_id', $selectedVehicleGeofence)->get();
        return $allGeoFence;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getVehicleId($userId)
    {
        $vehicleId = Vehicle::userId($userId);
        return $vehicleId;
    }

    /**
     * @param Request $request
     * @param $userId
     */
    public function vehicleAddFunctionForApiAndWeb(Request $request, $userId)
    {
        // Add vehicle in DB
        Vehicle::vehicleAddToDb($request->all(), $userId);

        $msg = 'You have added a new vehicle (' . $request->get('vehicle_name') . ')';

        Notification::updateUserIdInNotificationTable($userId, $msg, 'Vehicle');

        //update notification counter
        User::updateNotificationCount($userId);
    }

    /**
     * @param Request $request
     * @param $vehicleId
     * @param $userId
     */
    public function geoFenceAddFunctionForApiAndWeb(Request $request, $vehicleId, $userId)
    {
//     * It will make the other geofence inactive when we make "active" status to a specific vehicle
        GeoFence::checkGeofenceStatus($vehicleId);

        // Add geoFence in DB
        GeoFence::storeGeoFence($request->all(), $vehicleId);

        $msg = 'You have added a new GeoFence (' . $request->get('landmark_name') . ')';

        Notification::updateUserIdInNotificationTable($userId, $msg, 'GeoFence');

        //update notification counter
        User::updateNotificationCount($userId);
    }

    /**
     * @param $vehicleId
     * @param $geoFenceId
     * @param $userId
     */
    public function updateGeoFenceFunctionForApiAndWeb($vehicleId, $geoFenceId, $userId)
    {
//      * It will make the other geofence inactive when we make "active" status to a specific vehicle
        GeoFence::checkGeofenceStatus($vehicleId);

        // update vehicle in DB
        GeoFence::updateGeoFence(Input::all(), $geoFenceId, $vehicleId);

        $msg = 'You Updated your GeoFence to (' . Input::get('landmark_name') . ')';

        Notification::updateUserIdInNotificationTable($userId, $msg, 'GeoFence');

        User::updateNotificationCount($userId);
    }


}
