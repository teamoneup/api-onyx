<?php

namespace App\Http\Controllers\Auth;

use Mail;
use Auth;
use Input;
use App\User;
use App\Notification;
use App\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function postEmail(Request $request)
    {
        $this->validate($request, [
            'resetEmail' => 'required|email|max:255',
        ]);

        $validEmail = User::where('email',Input::get('resetEmail'))->get();
        if($validEmail->isEmpty())
        {
            $errorOnReset = 'Sorry! You are not registered with ONYX';
            return redirect()->back()->withErrors(['resetEmail'=>$errorOnReset]);
        }
        else
        {
            if(Input::all() != null) {
                $confirmation_code = Input::get('passToken');
                $input['email'] = Input::get('resetEmail');
//        $input['name'] = Input::get('email');

// Must not already exist in the `email` column of `users` table
                $rules = array('email' => 'required|email|max:255');

                $emailContent = array(
                    'token' => $confirmation_code,
                    'email' => $input['email'],

                );

                $data = PasswordReset::create
                ([
                    'token' => $confirmation_code,
                    'email' => $input['email'],
                ]);

                $validator = Validator::make($input, $rules);


                $collectionOfValidations = $validator->getMessageBag();

                if ($validator->fails()) {
                    $errorOnReset = $collectionOfValidations->get('email');
                    return view('password/email', compact('errorOnReset'));

                } else {
                    Mail::send('mails.resetPassword', $emailContent, function ($message) use ($emailContent) {
                        $message->from('onyx@gmail.com', 'Onyx GPS Password Reset Link');
                        //message body of mail is in registrationVerificationMail.blade file which is in mails folder
                        $message->to($emailContent['email'])->subject('Your ONYX Reset code Mr. : ');
                    });
                    $success = 'Your reset password link send to your mail successfuly. Please check your email.';
                    return view('auth/login', compact('success'));
//            return redirect()->back()->with('Success', 'Your reset password link send to your mail successfuly.');
                }
            }
            return redirect('auth/login');
        }

    }

    public function getReset($token)
    {
//        dd($token);

        return view('auth.reset',compact('token'));
    }
    public function postReset(Request $request)
    {
        $email =  PasswordReset::getEmailByConfirmationCode($request->get('passToken'));
        User::where('email', '=',$email)
            ->update(array
            (
                'password' => bcrypt($request['password']),
                'name' =>$request->name,

            ));
        $userId =  User::userId($email);
        Notification::updateUserIdInNotificationTable($userId, 'Password updated successfully ', 'password');

        //update notification counter
        User::updateNotificationCount($userId);

//        Delete token and email from Password Reset table at the mean time when password will change in Users table.
        PasswordReset::findOrFail($email)->delete();

        return redirect('auth/login');
    }

}
