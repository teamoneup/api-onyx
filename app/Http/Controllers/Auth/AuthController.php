<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\MapType;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use App\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        // creating random confirmation code
        $confirmation_code = str_random(30);
        // creating an array element for both confirm code and email
        $emailContent = array(
            'confirmation_code'=>$confirmation_code,
            'email'=>$data['email'],
            'name'=>$data['name'],
        );

       $userInfo = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code,
           // update confirmation code in user table

       ]);
        //get user id through email
        $userId = User::userId($data['email']);

      // update MapType Id in map_typs table for default map on Track Vehicle
       MapType::updateMapType('SATELLITE',$userId);

     // update confirmation notification on notification table
       Notification::updateUserIdInNotificationTable($userId,'Congratulation! You have created account and became a part of ONYX GPS Team successfully.','Registration');

        //mail send function is a laravel default function for sending mail
        Mail::send('mails.registrationVerificationMail', $emailContent, function($message) use($emailContent)
        {
            //message body of mail is in registrationVerificationMail.blade file which is in mails folder
            $message->to($emailContent['email'] )->subject('Your ONYX confirmation code Mr. : '.strtoupper($emailContent['name']));
        });
        return $userInfo;


    }
    public function confirmedMail($confirmation_code)
    {
//        update user status active default is inactive
        $data = User::userVerifiedSuccessfully($confirmation_code);
        return redirect('/');
    }


}
