<?php

namespace App\Http\Controllers;

use Auth;
use App\Vehicle;
use App\GeoFence;
use App\Http\Flash;
use App\Notification;
use App\Http\Requests;
use App\VehicleHistory;
use Illuminate\Http\Request;
use App\Http\Utilities\Days;
use App\Http\Utilities\TimeFormat;
use Illuminate\Support\Collection;
use App\Http\Requests\VehicleRequest;
use App\Http\Requests\GeoFenceRequest;
use App\Http\Requests\VehicleEditRequest;
use App\Http\Requests\GeoFenceEditRequest;

class VehicleController extends Controller
{


    /**
     * set middleware on vehicle controller
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');

    }


    /**
     * Adding a new vehicle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addVehicle()
    {
        $allVehicle = Vehicle::where('vehicle_status', '=', 'enable')->get();
        return view('vehicle.addVehicle', compact('allVehicle'));
    }

    /**
     * Manage Vehicle related action as add, edit, delete a vehicle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageVehicle()
    {
        $VehicleList = Vehicle::userId($this->userId)->orderBy('vehicle_name', 'asc')->paginate(10);
        $VehicleCounter = Vehicle::userId($this->userId)->get();

        // get imei of first vehicle
        $imei_number = Vehicle::getVehicleDetail($this->userId, 'user_id', 'imei_number');

        //now geting totle location update of first vehicle
        $counterForLocationUpdate = VehicleHistory::counterForLocationUpdate($imei_number)->get();
        return view('vehicle.managevehicle', compact('VehicleList', 'counterForLocationUpdate', 'VehicleCounter'));
    }

    /**
     * Saving a vehicle in datatbase.
     * @param VehicleRequest $request
     */
    public function saveVehicle(VehicleRequest $request)
    {
        $this->vehicleAddFunctionForApiAndWeb($request, $this->userId);

        flash()->success('Success!', 'Your Vehicle has been Added!');

        return redirect('managevehicle');
    }

    /**
     * Edit a vehicle
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editVehicle($id)
    {
        $editVehicle = Vehicle::findOrFail($id);

        return view('vehicle.editVehicle', compact('editVehicle'));
    }

    /**
     * Updating vehicle
     * @param VehicleEditRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateVehicle(VehicleEditRequest $request, $id)
    {
        $updatedId = Vehicle::findOrFail($id);

        $updatedId->fill($request->all())->save();

        $msg = 'You updated your vehicle to (' . $request->get('vehicle_name') . ')';

        Notification::updateUserIdInNotificationTable($this->userId, $msg, 'Vehicle');

        flash()->success('Success!', 'Your Vehicle info has been Updated!');

        return redirect('managevehicle');
    }

    /**
     * Delete a specific vehicle
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteVehicle($id)
    {
        $deleteId = Vehicle::findOrFail($id);

        $deleteId->delete();

        flash()->success('Success!', 'Your Vehicle Deleted Successfully!');

        return redirect('managevehicle');
    }

    /**
     * Add a new Geofence for a specific vehicle.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addGeoFence()
    {
        $daysList = Days::day();

        $vehicleList = Vehicle::userId($this->userId)->lists('vehicle_Name', 'vehicle_id');

        return view('geofence.addGeoFence', compact('vehicleList', 'daysList'));
    }

    /**
     * Manage a geofence action as delete,edit or adding a new geofence.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function managegeofence()
    {

        $allGeoFence = $this->getAllGeoFences($this->userId);

        $vehicleList = Vehicle::userId($this->userId)->lists('vehicle_Name', 'vehicle_id');

        return view('geofence.manageGeoFence', compact('vehicleList', 'allGeoFence'));
    }

    /**
     * TO display multiple or selected vehicle's geofence at a time.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function selectedGeoFence(Request $request)
    {
        $allGeoFence = $this->selectedGeoFences($request->get('selectedVehicleGeofence'));

        $vehicleList = Vehicle::userId($this->userId)->lists('vehicle_Name', 'vehicle_id');

        return view('geofence.manageGeoFence', compact('vehicleList', 'allGeoFence'));
    }

    /**
     * Saving a Geofence in database.
     * @param GeoFenceRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveGeoFence(GeoFenceRequest $request)
    {
        $this->geoFenceAddFunctionForApiAndWeb($request, $request->get('vehicle_id'), $this->userId);

        flash()->success('Success!', 'Your Geo Fence info Has been Added!');

        return redirect('managegeofence');
    }


    /**
     * Edit a Geofence
     * @param $geofenceid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editGeoFence($geofenceid)
    {
        $daysList = Days::day();

        $timeformat = TimeFormat::timeFormat();

        $vehicleList = Vehicle::userId($this->userId)
            ->lists('vehicle_Name', 'vehicle_id');

//        $selectedDays = GeoFence::selectDateTime($geofenceid, 'days');
        $selectedDayss = GeoFence::where('id', '=', $geofenceid)->value('days');
        $geofenceDetail = GeoFence::findOrFail($geofenceid);

        return view('geofence.editGeoFence', compact('geofenceDetail', 'vehicleList', 'daysList', 'timeformat', 'selectedDayss'));
    }

    /**
     * Updating a geofence.
     * @param GeoFenceRequest $request
     * @param $geofenceid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateGeoFence(GeoFenceRequest $request, $geofenceid)
    {
        $this->updateGeoFenceFunctionForApiAndWeb($request->get('vehicle_id'), $geofenceid, $this->userId);

        flash()->success('Success!', 'Your Vehicle info Has been edited!');

        return redirect('managegeofence');
    }

    /**
     * Delete a geofence
     * @param $delgeofence
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteGeoFence($delgeofence)
    {
        $deleteGeoFence = GeoFence::findOrFail($delgeofence);

        $deleteGeoFence->delete();

        return redirect('managegeofence');
    }



    /**
     * Display vehicle's history.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewHistory(Request $request)
    {

        $vehicleDetail = Vehicle::userId($this->userId)->get();

        $imei = Vehicle::findRow('vehicle_id', $request->get('vehicle_id'))->value('imei_number');

        $histories = VehicleHistory::findSelectedHistory($imei, $request->all());

        return view('history.viewHistory', compact('histories', 'vehicleDetail'));
    }

    /**
     * Report details of a user's Vehicles.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function manageReport()
    {

        return view('report.manageReport');
    }

}
