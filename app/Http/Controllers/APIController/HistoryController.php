<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 1/8/2016
 * Time: 4:27 PM
 */

namespace App\Http\Controllers\APIController;

use App\Vehicle;
use App\VehicleHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HistoryController extends ApiAuthController
{
    /**
     * @param Request $request
     * @return mixed|string
     */
    public function ViewHistoryThroughApi()
    {
        $userId = $this->getUserIdByEmailRequestForApi();

        //if email invalid or not provided then show error otherwise return response in json
        if ($userId != 0) {

            //set parameter for validation
            $validationOn = [
                'imei_number' => 'required|min:15',
            ];
            $validator = $this->validator(Input::all(), $validationOn);

            //check all the validation that's coming from request
            $collectionOfValidations = $validator->getMessageBag();

            if ($collectionOfValidations->get('imei_number') != null) {
                return $this->setStatusCode(400)
                    ->respondWithResponce($collectionOfValidations->get('imei_number'), 'Sorry');
            }

            else {
                //return response of given detail(show history)
                return json_encode(VehicleHistory::findSelectedHistory(Input::get('imei_number'), Input::all()));
            }
        }
        return $this->setStatusCode(401)
            ->respondWithResponce('Please provide a valid email address', 'Bad request');

    }

}