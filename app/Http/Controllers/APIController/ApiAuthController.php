<?php

namespace App\Http\Controllers\APIController;

use Auth;
use App\User;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;


class ApiAuthController extends OnyxJsonHandleController
{
    protected $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function postLogin(Request $request)
    {
        $authStatus = Auth::attempt($request->only(['email', 'password']), $request->has('remember'));

        if (!$authStatus) {

            return $this->setStatusCode(400)

                ->respondWithResponce('Invalid Email or Password', 'Error');
        }
        else {

            return $this->redirectOnSuccess($request);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        //set parameter for validation
        $validationOn = [
            'name'  => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ];

        $validator = $this->validator($request->all(), $validationOn);

        return $this->respondWithValidation($request, $validator);
    }

    /**
     * @return mixed
     */
    public function postLogout()
    {
        Auth::logout();

        return $this->setStatusCode(200)
            ->respondWithResponce('Thank you for using our services', 'Thank you');
    }


}
