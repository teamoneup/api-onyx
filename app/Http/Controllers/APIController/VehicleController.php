<?php

namespace App\Http\Controllers\APIController;

use App\User;
use App\Vehicle;
use App\GeoFence;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class VehicleController extends ApiAuthController
{
    /**
     * @return mixed|string
     *
     */
    public function index()
    {
        $userId = $this->getUserIdByEmailRequestForApi();

        $imei = Input::get('imei_number');

        $vehicleName = Input::get('vehicleName');

        $selectedVehiclesInArrayById = explode(',', $vehicleName);

        $selectedVehiclesInArrayByImei = explode(',', $imei);

        //if email invalid or not provided then show error otherwise return response in json
        if ($userId != 0) {

            if ($vehicleName || $imei) {

                if ($imei) {

                    // selected imei_numbers by Vehicle model
                    return json_encode(Vehicle::whereIn('imei_number', $selectedVehiclesInArrayByImei)->get());

                }

                return json_encode(Vehicle::whereIn('vehicle_name', $selectedVehiclesInArrayById)->get());
            }

            return json_encode(Vehicle::where('user_id', $userId)->get());
        }
        return $this->setStatusCode(401)
            ->respondWithResponce('Please provide a valid email address', 'Bad request');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $userId = $this->getUserIdByEmailRequestForApi();

        if ($userId != 0) {

            $respond = $this->requestForApiVehicleAddUpdateDetete();

            if ($respond != null) {

                return $respond;

            }
            else {

                $this->vehicleAddFunctionForApiAndWeb($request, $userId);

                return $this->setStatusCode(200)
                    ->respondWithResponce('Your vehicle added successfully', 'Added');
            }
        }
        return $this->setStatusCode(401)
            ->respondWithResponce('Please provide a valid email address', 'Bad request');
    }

    /**
     * @param $email
     * @return mixed
     */
    public function update($email)
    {

//        get userid by email
        $userId = User::userId($email) ?: 0;

        $getVehicleIdThroughImeiNo = Vehicle::getVehicleIdThroughImeiNo(Input::get('imei_number'),$userId);

        if ($userId != 0 && $getVehicleIdThroughImeiNo != 0) {

            $respond = $this->requestForApiVehicleAddUpdateDetete();

            if ($respond != null) {
                return $respond;
            } else {

                $updatedId = Vehicle::findOrFail($getVehicleIdThroughImeiNo);

                $updatedId->fill(Input::all())->save();

                return $this->setStatusCode(200)
                    ->respondWithResponce('Your vehicle updated successfully', 'Updated');
            }
        }

        return $this->setStatusCode(400)
            ->respondWithResponce('You are trying to update wrong vehicle', 'Sorry');

    }

    public function destroy($email)
    {

//      get userid by email
        $userId = User::userId($email) ?: 0;

        if ($userId != null) {

            $respond = $this->requestForApiVehicleAddUpdateDetete();

            if ($respond != null) {

                return $respond;

            } else {

                //      get vehicle id by landmark name
                $vehicleId = Vehicle::getVehicleIdThroughImeiNo(Input::get('imei_number'),$userId);
                //check geofence existence or not
                if ($vehicleId != null) {

                    $deleteVehicle = Vehicle::findOrFail($vehicleId);

                    $deleteVehicle->delete();

                    return $this->setStatusCode(200)
                        ->respondWithResponce('Your Vehicle has been deleted successfully', 'Deleted');
                }

                return $this->setStatusCode(400)
                    ->respondWithResponce('This Vehicle is not found or already deleted', 'Sorry');
            }
        }
        return $this->setStatusCode(401)
            ->respondWithResponce('Please enter a valid email address this email id dose not match our records', 'Sorry');
    }


    /**
     * @return mixed
     */
    public function requestForApiVehicleAddUpdateDetete()
    {
        //set parameter for validation
        $validationOn = [
            'vehicle_name' => 'required',
            'imei_number' => 'required|min:15',
        ];
        $validator = $this->validator(Input::all(), $validationOn);

        //check all the validation that's coming from request
        $collectionOfValidations = $validator->getMessageBag();

        if ($collectionOfValidations->get('vehicle_name') != null) {

            return $this->setStatusCode(400)
                ->respondWithResponce($collectionOfValidations->get('vehicle_name'), 'Sorry');
        }

        elseif ($collectionOfValidations->get('imei_number') != null) {

            return $this->setStatusCode(400)
                ->respondWithResponce($collectionOfValidations->get('imei_number'), 'Sorry');
        }
    }
}
