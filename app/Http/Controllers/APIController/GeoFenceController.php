<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 1/7/2016
 * Time: 6:57 PM
 */

namespace App\Http\Controllers\APIController;

use App\GeoFence;
use App\User;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class GeoFenceController extends ApiAuthController
{
    public function index()
    {
        $userId = $this->getUserIdByEmailRequestForApi();

        $selectedGeoFenceInArrayById = explode(',', Input::get('vehicleId'));

        $selectedGeoFenceInArrayByImei = explode(',', Input::get('imei'));

        //if email invalid or not provided then show error otherwise return response in json
        if ($userId != 0) {
            if (Input::get('vehicleId') || Input::get('imei')) {

                if (Input::get('imei')) {

                    // selectedGeoFenceInArrayById by geo fence
                    $selectedGeoFenceInArrayById = Vehicle::getVehicleIdThroughImeiNoInArray($selectedGeoFenceInArrayByImei)
                        ->get(['vehicle_id']);
                }
                return json_encode($this->selectedGeoFences($selectedGeoFenceInArrayById));
            }

            return json_encode($this->getAllGeoFences($userId));
        }
        return $this->setStatusCode(401)
            ->respondWithResponce('Please provide a valid email address', 'Bad request');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        list($checkDateFromValidOrNot,$checkDateToValidOrNot, $checkTimeFromValidOrNot, $checkTimeToValidOrNot) = $this->CheckDateOrTime($request);

        if ($checkDateToValidOrNot != true || $checkDateFromValidOrNot != true) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide a valid Date format like yyyy-mm-dd', 'Bad format');
        }

        if ($checkTimeFromValidOrNot != true || $checkTimeToValidOrNot != true) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide a valid Time format like h:mm:ss AM/PM', 'Bad format');
        }

        $userId = $this->getUserIdByEmailRequestForApi() ?: 0;

        $vehicleId = $this->getVehicleIdByImeiOrVehicleId($userId);

        if ($vehicleId != 0 && $userId != 0) {

            $respond = $this->requestForApiGeofenceAddUpdateDelete();

            if ($respond != null) {

                return $respond;

            } else {

                // save geo fence for api
                $this->geoFenceAddFunctionForApiAndWeb($request, $vehicleId, $userId);

                return $this->setStatusCode(200)
                    ->respondWithResponce('Your GeoFence added successfully', 'Added');
            }
        }
        return $this->setStatusCode(401)
            ->respondWithResponce('Please provide a valid email address and vehicle id or imei no', 'Bad request');
    }

    /**
     * @param $email
     * @return mixed
     */
    public function update($email)
    {
        list($checkDateFromValidOrNot, $checkDateToValidOrNot, $checkTimeFromValidOrNot, $checkTimeToValidOrNot) = $this->CheckDateOrTime(Input::all());


        if ($checkDateToValidOrNot != true || $checkDateFromValidOrNot != true) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide a valid Date format like yyyy-mm-dd', 'Bad format');
        }

        if ($checkTimeFromValidOrNot != true || $checkTimeToValidOrNot != true) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide a valid Time format like h:mm:ss AM/PM', 'Bad format');
        }

//        get userid by email
        $userId = User::userId($email) ?: 0;

//      get vehicle id by landmark name
        $vehicleId = Vehicle::getVehicleIdThroughImeiNo(Input::get('imei_number'), $userId);

//      get geofence id by landmark name
        $geoFenceId = GeoFence::getGeoFenceIdByVehicleIdAndLandmarkName($vehicleId, Input::get('landmark_name'));


//         need vehicle id for update selected geofence

        if ($vehicleId == 0) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide a valid imei number or vehicle id', 'Bad request');

        } elseif ($userId == 0) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide a valid email address ', 'Bad request');

        } elseif ($geoFenceId == 0) {

            return $this->setStatusCode(401)
                ->respondWithResponce('Please provide an existed and exact landmark name ', 'Bad request');

        } else {

            $respond = $this->requestForApiGeofenceAddUpdateDelete();

            if ($respond != null) {

                return $respond;

            } else {

                $this->updateGeoFenceFunctionForApiAndWeb($vehicleId, $geoFenceId, $userId);

                return $this->setStatusCode(200)
                    ->respondWithResponce('Your GeoFence updated successfully', 'Added');
            }
        }

    }

    /**
     * @return int
     */
    public function getVehicleIdByImeiOrVehicleId($userId)
    {
        $vehicleId = '';

        $imei = Input::get('imei_number') ?: 0;

        $vehicleIdCheck = Vehicle::find(Input::get('vehicle_id'));

        if ($imei != 0) {

            $vehicleId = Vehicle::getVehicleIdThroughImeiNo($imei, $userId);

            return $vehicleId;

        }

        elseif ($vehicleIdCheck != null) {

            $vehicleId = Input::get('vehicle_id') ?: 0;

        }

        return $vehicleId;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function destroy($email)
    {

//      get userid by email
        $userId = User::userId($email) ?: 0;

        if ($userId != null) {

            $respond = $this->requestForApiGeofenceAddUpdateDelete();

            if ($respond != null) {

                return $respond;

            } else {

                //      get vehicle id by landmark name
                $vehicleId = $this->getVehicleIdByImeiOrVehicleId($userId);

                //      get geofence id by landmark name
                $geoFenceId = GeoFence::getGeoFenceIdByVehicleIdAndLandmarkName($vehicleId, Input::get('landmark_name'));

                //check geofence existence or not
                if ($geoFenceId != null) {

                    $deleteGeofance = GeoFence::findOrFail($geoFenceId);

                    $deleteGeofance->delete();

                    return $this->setStatusCode(200)
                        ->respondWithResponce('Your Geo-fence has been deleted successfully', 'Deleted');
                }

                return $this->setStatusCode(200)
                    ->respondWithResponce('This fencing is not found or already deleted', 'Sorry');

            }

        }
    }

    /**
     * @return mixed
     */
    public function requestForApiGeofenceAddUpdateDelete()
    {
        //set parameter for validation
        $validationOn = [
            'landmark_name' => 'required',
//            'imei_no' => 'required',
        ];

        $validator = $this->validator(Input::all(), $validationOn);

        //check all the validation that's coming from request
        $collectionOfValidations = $validator->getMessageBag();

        if ($collectionOfValidations->get('landmark_name') != null) {

            return $this->setStatusCode(400)
                ->respondWithResponce($collectionOfValidations->get('landmark_name'), 'Sorry');
        }
//        elseif ($collectionOfValidations->get('imei_no') != null) {
//
//            return $this->setStatusCode(400)
//                ->respondWithResponce($collectionOfValidations->get('imei_no'), 'Sorry');
//        }
    }

    /**
     * @param $date
     * @param array $requestForDate
     * @return bool
     */
    public function checkDateValidOrNot($date, array $requestForDate)
    {
        if (array_key_exists($date, $requestForDate)) {

            // Here preg_match check YYYY:MM:DD
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $requestForDate[$date])) {

                return true;

            }
            else {

                return false;
            }
        }
        return true;
    }

    /**
     * @param $time
     * @param array $requestForDate
     * @return bool
     */
    public function checkTimeValidOrNot($time, array $requestForDate)
    {
        if (array_key_exists($time, $requestForDate)) {

            // Here preg_match check H:MM:SS AM/PM
            if (preg_match("/^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$/", $requestForDate[$time])) {

                return true;

            }

            else {

                return false;
            }
        }
        return true;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function CheckDateOrTime(Request $request)
    {
        $checkDateFromValidOrNot = $this->checkDateValidOrNot('date_from', $request->all());

        $checkDateToValidOrNot = $this->checkDateValidOrNot('date_to', $request->all());

        $checkTimeFromValidOrNot = $this->checkTimeValidOrNot('time_from', $request->all());

        $checkTimeToValidOrNot = $this->checkTimeValidOrNot('time_to', $request->all());

        return array($checkDateFromValidOrNot, $checkDateToValidOrNot, $checkTimeFromValidOrNot, $checkTimeToValidOrNot);
    }

}