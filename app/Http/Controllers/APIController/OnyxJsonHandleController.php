<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 1/6/2016
 * Time: 1:41 PM
 */

namespace App\Http\Controllers\APIController;


use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\Response as errorReporting;
use App\Http\Controllers\Controller;
use App\OnyxApi\ValidationOfOnyxApi;


class OnyxJsonHandleController  extends ValidationOfOnyxApi
{
    protected $statusCode = errorReporting::HTTP_OK;

    /**
     * @return mixed
     * @internal param $statusCode
     */

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $message
     * @param $respondType
     * @return mixed
     * @internal param $statusCode
     */
    public function respondWithResponce($message,$respondType)
    {
        return $this->respond(
            [
                $respondType => [
                    'message' => $message,
                    'status code' => $this->getStatusCode()
                ]
            ]
        );
    }

    /**
     * @param $data
     * @param array $header
     * @return mixed
     */
    public function respond($data, $header = [])
    {
        return response()->json($data, $this->getStatusCode(), $header);
    }

/**
     * @return int
     */
    public function getUserIdByEmailRequestForApi()
    {
        // get email from url
        $email = Input::get('email');

        //get user id by email (check email id valid or not)
        $userId = User::userId($email) ?: 0;
        return $userId;
    }

}