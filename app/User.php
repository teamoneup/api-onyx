<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Http\Request;
use App\Vehicle;
use Auth;
use App\Http\Controllers\SocialLite;
class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'mobile', 'address', 'member_since', 'confirmation_code','confirmed','social_passcode'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }
//
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    public function getName()
    {
        if ($this->first_name && $this->last_name) {
            return "{ $this->first_name} {$this->last_name}";
        }

        if ($this->first_name) {
            return $this->first_name;
        }

        return null;
    }

    public function getNameOrUserName()
    {
        return $this->getName() ?: $this->username;
    }

    public function getFirstNameOrUserName()
    {
        return $this->first_name ?: $this->username;
    }

//    public static function updateConfirmationCode($confirmation_code, $email)
//    {
//        return static::where('email', $email)
//            ->update(['confirmation_code' => $confirmation_code]);
//    }
//    public static function findUserIdBySecretKey($confirmation_code)
//    {
//        return static::where('confirmation_code', $confirmation_code)->value('id');
////            ->update(['confirmed' => 1]);
//    }

    public static function userVerifiedSuccessfully($confirmation_code)
    {
        return static::where('confirmation_code', $confirmation_code)
            ->update(['confirmed' => 1]);
    }

    public static function    checkConfirmation($email)
    {
        return static::where('email', '=', $email)->value('confirmed');
    }

    public static function StoreUserProfile(Request $request, $user_profile_image,$user_profile_image_Th, $userId)
    {
        return static::where('id', $userId)
            ->update(['name' => $request->get('name'),
                'address' => $request->get('address'),
                'email' => $request->get('email'),
                'mobile' => $request->get('mobile'),
                'user_profile_image' => $user_profile_image,
                'user_thumbnails' => $user_profile_image_Th]);
    }

    public static function userProfilePicture($userId, $image)
    {
        return static::where('id', '=', $userId)->value($image);
    }

    public function notificationsOfUser()
    {
        return $this->hasMany('App\Notification');
    }

    public static function userId($email)
    {
        return static::where('email','=', $email)->value('id');
    }

    public static function  updateNotificationCount($userId)
    {
        $oldCount =  static::where('id', '=', $userId)->value('user_notification_counter');
        $newCount = $oldCount + 1;
        return static::where('id', '=', $userId)
            ->update(['user_notification_counter' => $newCount]);
    }
    public static function updateNotificationCountToZero($userId)
    {
        return static::where('id', '=', $userId)
            ->update(['user_notification_counter' => 0]);
    }
    public static function saveUserRegistration(Request $request)
    {
        return static:: create([
            'email' => $request['email'],
            'password' => $request['passwords'],
            'name' => $request['name'],
        ]);
    }

}
