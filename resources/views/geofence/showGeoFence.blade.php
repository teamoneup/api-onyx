@inject('getVehicleDetail','App\Vehicle)
<table class="table table-responsive bg-primary">
    <tr>
        <th class="text-center">Vehicle Name</th>
        <th class="text-center">Landmark Name</th>
        <th class="text-center">Status</th>
        <th class="text-center">Action</th>
    </tr>
    @foreach($allGeoFence as $allGeoFenceLists)
{{--        @foreach($vehicleId as $vehicleIds)--}}
{{--            @if($vehicleIds->vehicle_id == $allGeoFenceLists->vehicle_id)--}}
                <tr>
                    <td class="text-center">{{$getVehicleDetail->getVehicleDetail($allGeoFenceLists->vehicle_id,'vehicle_id','vehicle_name')}}</td>
                    <td class="text-center">{{$allGeoFenceLists->landmark_name}}</td>
                    <td class="text-center">{{$allGeoFenceLists->status}}</td>
                    <td class="text-center">
                        <a href="{{url('editgeofence',$allGeoFenceLists->id)}}">
                            <span class="btn btn-success glyphicon glyphicon-pencil"></span></a>
                        <a href="{{url('deletegeofence',$allGeoFenceLists->id)}}"><span
                                    class="btn btn-danger glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
            {{--@endif--}}
        {{--@endforeach--}}
    @endforeach
</table>