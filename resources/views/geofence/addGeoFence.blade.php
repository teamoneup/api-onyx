@extends('layout')

@section('title')
    |Add Geofence|
    @stop

    @section('style')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->

    <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>

@stop
@section('content')

    <div class="container">
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light " id="form_wizard_1">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> Simple Steps for Add Geofence -
                                                    <span class="step-title"> Step 1 of 4 </span>
                                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">

                            {!! Form::open(['url'=>'savegeofence','class' => 'form-horizontal','id'=>'submit_form'], array('role'=>'form')) !!}

                            <div class="form-wizard">
                                <div class="form-body">
                                    <ul class="nav nav-pills nav-justified steps">

                                        <li>
                                            <a href="#tab1" data-toggle="tab" class="step">
                                                <span class="number"> 1 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i>Select Vehicle</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#tab2" data-toggle="tab" class="step">
                                                <span class="number"> 2 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i>Draw Geo Fence </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#tab3" data-toggle="tab" class="step active">
                                                <span class="number"> 3 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i>Select Date & Time</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#tab4" data-toggle="tab" class="step active"
                                               id="show_save">
                                                <span class="number"> 4 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i>Status</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="bar" class="progress progress-striped" role="progressbar">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="alert alert-danger display-none">
                                            <button class="close" data-dismiss="alert"></button>
                                            You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-success display-none">
                                            <button class="close" data-dismiss="alert"></button>
                                            Your New GeoFence created Successfully!
                                        </div>
                                        <div class="tab-pane active" id="tab1">
                                            <h3 class="block">Select your Vehicle</h3>

                                            <div class="form-group">

                                                <label class="control-label col-md-3">Select Vehicle
                                                </label>

                                                <div class="col-md-4">
                                                    {!! Form::select('vehicle_id',$vehicleList,null, array('class' => 'bs-select form-control','required')) !!}
                                                </div>

                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab2">
                                            <h3 class="block">Draw Geo Fence</h3>
                                                    <span style="color: Red"><span
                                                                style="color: Red"><b>Note: </b></span>Please Draw Four Points to Make Your Geofence on Map. No worries, Start & End Points will Automatically Connect. </span>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="GeoMapInIt">
                                                        <div id="map_canvas"
                                                             style="width:100%; height:500px; border:1px solid gray"></div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Provide name of Geofence
                                                    <span class="required"> * </span>
                                                </label>

                                                <div class="col-md-4">

                                                    <input type="hidden" name="geo_fences_coordinates"
                                                           class="addressArea" required>
                                                    {!! Form::text('landmark_name',null, array('class' => 'form-control','placeholder'=>'Geofence Name', 'required')) !!}
                                                </div>
                                            </div>
                                        </div>


                                        <div class="tab-pane set-tab3" id="tab3">
                                            <h3 class="block">Select Date & Time</h3>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Date From </label>
                                                            <span class="help-block">
                                                            <div class="fix_width input-group input-large date-picker input-daterange"
                                                                 data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                                                <input type="text" class="form-control"
                                                                       name="date_from">
                                                                <span class="input-group-addon"> to </span>
                                                                <input type="text" class="form-control" name="date_to">
                                                            </div>
                                                            </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-1"></div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Select Days</label>

                                                        <div class="input-icon">
                                                            {!! Form::select('days[]',$daysList,null, array('multiple','class' => 'form-control bs-select')) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Time From</label>

                                                        <div class="input-icon">
                                                            <i class="fa fa-clock-o"></i>
                                                            <input type="text" name="time_from"
                                                                   class="form-control timepicker timepicker-default defaultTimeBoxFrom">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Time To</label>

                                                        <div class="input-icon">
                                                            <i class="fa fa-clock-o"></i>
                                                            <input type="text" name="time_to"
                                                                   class="form-control timepicker timepicker-default defaultTimeBoxTo">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="tab4">
                                            <h3 class="block">Status Details</h3>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Current Status
                                                </label>

                                                <div class="col-md-4">
                                                    {!! Form::select('status',['active'=>'active','disable'=>'disable'],null, array('class' => 'form-control bs-select')) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> Back </a>
                                        <a href="javascript:;" class="btn btn-outline green button-next "
                                           onclick="mapInit()">
                                            Continue
                                            <i class="fa fa-angle-right"></i>
                                        </a>

                                        {!! Form::submit('Save Geofence', ['class' => 'btn btn-primary button-submit','onclick'=>'vechicleGeofence();getGeoFenceCoordinates();']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                        @include('errors.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->

    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>

    @include('partial.geoFenceMap')
    @include('partial.footer')

    @stop{{--content--}}

    @section('script')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>

    <script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

    <script>
        $(".defaultTimeBoxFrom").dblclick(function () {
            $('.defaultTimeBoxFrom').val("")
        })
        $(".defaultTimeBoxTo").dblclick(function () {
            $('.defaultTimeBoxTo').val("")
        })
        function vechicleGeofence() {
            $("form#submit_form").submit(function () {
                $.ajax({
                    url: 'savegeofence',
                    type: 'post',
                    cache: false,
                    dataType: 'json',
                    data: $('form#submit_form').serialize(),
                    beforeSend: function () {
//                        self.location['managegeofence']
//                        location = self['managegeofence'].href
                        location.assign("managegeofence")
//                        document.location.replace("managegeofence")
                    },
                });

            });
        }
    </script>

@stop
