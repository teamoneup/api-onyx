<div class="form-group">
    {!! Form::label('vehicle_id','Select Vehicle', array('class' => 'col-lg-4 control-label')) !!}
    <div class="col-lg-8">
        {!! Form::select('vehicle_id',$vehicleList,null, array('class' => 'form-control bs-select')) !!}
    </div>
</div>
<div class="form-group">
    <span class="col-lg-4 control-label"> Checkpoint <i class="text-danger">*</i></span>

    <div class="col-lg-8">
        {!! Form::text('landmark_name',null, array('class' => 'form-control','placeholder'=>'Checkpoint/Landmark Name')) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('date_from','Date', array('class' => 'col-lg-4 control-label')) !!}
    <div class="col-lg-8">
        <div class="date-picker input-daterange">
            From :{!! Form::text('date_from',null, array('class' => 'text-left form-control','readonly')) !!}
            To :{!! Form::text('date_to',null, array('class' => 'form-control','readonly')) !!}
        </div>
    </div>
</div>


<div class="form-group">
    {!! Form::label('date_from','Time', array('class' => 'control-label col-lg-4')) !!}
    <div class="col-lg-8">
        From :{!! Form::text('time_from',null, array('class' => ' form-control timepicker timepicker-default defaultTimeBox','placeholder'=>'Time From')) !!}
        To :{!! Form::text('time_to',null, array('class' => ' form-control timepicker timepicker-default defaultTimeBox','placeholder'=>'Time To')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('days','Days', array('class' => 'col-lg-4 control-label')) !!}
    <div class="col-lg-8">
        @if($editId != 'null')
            {!! Form::select('days[]',$daysList,explode(',',$selectedDayss), array('multiple','class' => 'form-control bs-select')) !!}
        @else
            {!! Form::select('days[]',$daysList,null, array('multiple','class' => 'form-control bs-select')) !!}
        @endif
    </div>
</div>



<div class="form-group">
    {!! Form::label('status','Status', array('class' => 'col-lg-4 control-label')) !!}
    <div class="col-lg-8">
        {!! Form::select('status',['active'=>'active','disable'=>'disable'],null, array('class' => 'form-control bs-select')) !!}
    </div>
</div>
