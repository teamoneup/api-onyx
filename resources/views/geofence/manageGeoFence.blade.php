@inject('vehicle_list','App\Vehicle')
@extends('layout')
@section('style')
    <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet"
          type="text/css"/>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    @stop
    @section('content')

            <!-- BEGIN CONTAINER -->

    <!-- BEGIN CONTENT -->
    <div class="page-content" style="margin-top: 40px;">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                            <span data-counter="counterup"
                                                  data-value="{{count($allGeoFence)}}">{{count($allGeoFence)}}</span>
                                    </div>
                                    <div class="desc"> Number of Geofence</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="1,5">0</span>M$
                                    </div>
                                    <div class="desc"> Total Profit</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="20">0</span>
                                    </div>
                                    <div class="desc"> New Orders</div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="more" href="{{url('addgeofence')}}">
                                <div class="dashboard-stat purple">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> +
                                            <span data-counter="counterup"></span>
                                        </div>
                                        <div class="desc"> Add Geofence</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <div class="col-md-4">
                                {!! Form::open(['class' => 'form-horizontal','id'=>'submit_form']) !!}
                                {!! Form::select('selectedVehicleGeofence[]', $vehicleList, null, ['class' => 'bs-select form-control','multiple'=>'multiple'])  !!}
                            </div>
                            <div class="col-md-3">
                                {!! Form::submit('Show', ['class' => 'btn btn-default']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5px;">
                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-car"></i>Manage Geofence </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: auto;"> <b># </th>
                                                <th><b> Vehicle Name</b> </th>
                                                <th><b> Geofence Name</b> </th>
                                                <th><b> Days/Date</b> </th>
                                                <th><b> Status</b> </th>
                                                <th><b> Maps</b> </th>
                                                <th><b> Action</b> </th>
                                            </tr>
                                            </thead>
                                            <?php static $i = 1;?>
                                            @foreach($allGeoFence as $VehicleListDetails)
                                            <tbody>
                                            <tr>
                                                <td style="width: auto;"><?php echo $i++; ?></td>
                                                <td>{{$vehicle_list->VehicleListDetails($VehicleListDetails->vehicle_id)}}</td>
                                                <td> {{$VehicleListDetails->landmark_name}}</td>
                                                <td> {{$VehicleListDetails->days}}</td>
                                                <td style="width:40px;"> {{$VehicleListDetails->status}}</td>
                                                <td>
                                                    {!! $count = null !!}
                                                    <?php static $j = 0;?>
                                                    <?php $data = [];
                                                    $data = explode('|', $VehicleListDetails->geo_fences_coordinates);
                                                    ?>
                                                    <span class="hidden"> {!! $counter = count(explode('|',$VehicleListDetails->geo_fences_coordinates))  !!}</span>

                                                    <div class="col-md-12" class="GeoMapInIts">
                                                        <div id="googleMap{{$j}}" style="width:250px; height:140px; border:1px solid gray">
                                                            @include('partial.geoFenceDraw')

                                                        </div>
                                                        <?php $j++; ?>
                                                    </div>
                                                </td>
                                                <td class="" style="width: 18%;">
                                                    <a class="btn green" href="{{url('editgeofence')}}/{{$VehicleListDetails->id}}">EDIT</a>
                                                    <a class=" btn red sbold" data-toggle="modal" href="#geofence">REMOVE</a>
                                                    @include('history.modal_confirmation')

                                                </td>
                                            </tr>

                                            </tbody>
                                                @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>

            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->

        <!-- END CONTENT -->
        <br><br><br>
    </div>
    @include('partial.footer')
@stop

@section('script')

    {!! Html::script('js/jquery.dataTables.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                    );

                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
        });
    </script>

    {{--<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>--}}
    {{--select--}}
    <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
    {{--select end--}}


    <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    {{--<script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>--}}

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/maps-google.min.js" type="text/javascript"></script>



    {{--Modals--}}
    <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
@stop
