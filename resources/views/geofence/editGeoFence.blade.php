@extends('edit_layout')

@section('title')
    |Add Geofence|
    @stop

    @section('style')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->

    <link href="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>


@stop
@section('content')
    <div class="container">
        <div class="row ">
            <div class="col-lg-12">
                <div class="col-lg-4">
                    {{--<div>--}}
                    {!! Form::model($geofenceDetail,['method' => 'post','class' => 'form-horizontal']) !!}

                    @include('geofence.geoFenceForm',['editId'=>'notNull'])
                    <div class="form-group">
                        <div class="col-lg-3">
                            <input type="hidden" name="geo_fences_coordinates"
                                   class="addressArea" required>
                        </div>
                        <div class="col-lg-9">
                            {!! Form::submit('Update Geofence', ['class' => 'btn btn-info closeFancy pull-right','onclick'=>'getGeoFenceCoordinates();']) !!}
                        </div>

                    </div>
                    {!! Form::close() !!}
                    {{--</div>--}}


                    {!! $i = null !!}
                    {!! $count = null !!}
                    <?php $data = [];
                    $data = explode('|', $geofenceDetail->geo_fences_coordinates);
                    ?>
                    <span class="hidden"> {!! $counter = count(explode('|',$geofenceDetail->geo_fences_coordinates))  !!}</span>

                </div>
                <div class="col-lg-8">
                    @include('errors.list')
                    {{--<div class="col-lg-1" ></div>--}}

                    <div class="GeoMapInIt">
                        <span class="text-danger">Note : You can re-draw your geofence here otherwise it will remain same </span>

                        <div id="map_canvas"
                             style="width:100%; height:410px; border:1px solid gray"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partial.footer')
    @stop

    @section('script')
    @include('partial.geoFenceMap')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js"
            type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="../assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>

    <script src="../assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
            type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="../assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
    <script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

    <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

@stop
