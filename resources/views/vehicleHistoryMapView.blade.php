@extends('edit_layout')
@include('partial.OldTopNavbar')


@section('title')
    History On Map
@stop

@section('content')
    {{--<button type="button" class="form-control btn btn-primary col-md-2" id="start" onclick="startMoving()">Start</button>--}}
    <?php

    static $first = 1;
    $firstLat = $firstLng = null;
    $firstLng = null;

    static $last = 1;
    $lastLat = null;
    $lastLng = null;
    foreach ($histories as $element) {

        if ($first == 1) {
            $firstLat = $element->latitude;
            $firstLng = $element->longitude;
            $start = 1;
        } elseif ($last == $histories->count()) {
            $lastLat = $element->latitude;
            $lastLng = $element->longitude;
        }
        $first++;
        $last++;
    } ?>
    <div class="col-lg-12">
        {!! Form::open() !!}
        <div class="form-group">
            <div class="col-md-6">
                <p class="SearchByDateFromTO">

                    <sapn>From <input type="text" class="date start " name="date_start"></sapn>
                    <span> <input type="text" class="time start ui-timepicker-input" name="time_start"
                                  autocomplete="off"> </span>
                    <span>To  <input type="text" class="time end ui-timepicker-input" name="time_end"
                                     autocomplete="off"></span>
                    <span> <input type="text" class="date end" name="date_end"></span>
                </p>

            </div>
            <div class="col-lg-2">
                <button type="submit" class="btn btn-default col-lg-12" name="history" value="tableHistory">Find History
                </button>

                {{--<button type="submit" class="btn btn-default col-sm-12">View History</button>--}}
            </div>
            <div class="col-lg-2">
                <button type="submit" class="btn btn-default col-lg-12" name="history" value="viewOnMap">View On Map
                </button>

            </div>
            <div class="col-lg-2">
                <a href="">
                    <button type="reset" class="btn btn-default col-sm-12">Clear</button>
                </a>
            </div>
            {{--<div class="col-lg-2">--}}
            {{--<button onclick="window.print();" type="button" class=""><i class="fa fa-print" style="font-size: 30px; color: black;"></i></button>--}}
            {{--</div>--}}
        </div>
        {!! Form::close()!!}
    </div>
    <div class="col-lg-12">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta charset="utf-8">
        <style>
            html, body {
                width: 100%;
                height: 90%;
                /*margin: 0;*/
                /*padding: 0;*/
            }

            #map {
                height: 100%;
            }
        </style>

        <div id="map"></div>
    </div>
@stop
<script>
    // This example adds an animated symbol to a polyline.

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 28.010548122529617, lng: 73.3342127153476},
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // Define the symbol, using one of the predefined paths ('CIRCLE')
        // supplied by the Google Maps JavaScript API.
        var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            fillColor: 'white',
            fillOpacity: 0.8,
            scale: 1,
            strokeColor: 'green',
            strokeWeight: 8

        };
        gmarkers = [];

        for (var i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
        var sLatitude = '<?php echo $firstLat ?>';
        var sLongitude = '<?php echo $firstLng ?>';

        var eLatitude = '<?php echo $lastLat ?>';
        var eLongitude = '<?php echo $lastLng ?>';
        var Distance;
        Distance =   distance(sLatitude, sLongitude, eLatitude, eLongitude, 'k')

        createMarker(sLatitude,sLongitude,'green','Start point of taxi',Distance)

//        var sCoord = new GeoCoordinate(sLatitude, sLongitude);
//        var eCoord = new GeoCoordinate(eLatitude, eLongitude);
//
////        var distance;
////        distance =  ;
//        consol.log(sCoord.GetDistanceTo(eCoord));
        function createMarker(lat, lng, color, label,Distance) {
            gmarkers = [];
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                title: label,
                icon: 'http://maps.google.com/mapfiles/ms/icons/'+color+'.png',
            });
            marker.myname = label;
            gmarkers.push(marker);
        }
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(eLatitude, eLongitude),
            map: map,
            title: 'Destination point and Distance : '+ Distance,
            icon: 'http://maps.google.com/mapfiles/ms/icons/red.png',
        });


        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(contentString);
            infowindow.open(map,marker);
        });

        {{--{{dd($history->taxi_long)}}--}}
        // Create the polyline and add the symbol to it via the 'icons' property.
        var line = new google.maps.Polyline({
//            path: [
//                {lat: 28.010548122529617, lng: 73.3342127153476},
//                {lat: 22.110548122549617, lng: 72.3342127153476},
//                {lat: 28.210548122559617, lng: 75.3342127153476},
//                {lat: 29.010548122559617, lng: 76.3342127153476},
//                {lat: 29.210548122579617, lng: 75.3342127153476}
//            ],
            // Predefine all the paths

            <?php


               echo'path: [';
               foreach($histories as $history)
                {
                echo ' { lat: '.$history->latitude.', lng: '.$history->longitude.'},';
                }
                echo '],';
            ?>

        icons: [{
                icon: lineSymbol,
                offset: '100%'
            }],
            map: map
        });

//        function startMoving()
//        {
        animateCircle(line,Distance);
//        }
//
    }
    function distance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var radlon1 = Math.PI * lon1/180
        var radlon2 = Math.PI * lon2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist
    }

    // Use the DOM setInterval() function to change the offset of the symbol
    // at fixed intervals.
    function animateCircle(line,Distance) {
        var count = 0;
        window.setInterval(function () {
            count = (count + 1) % 200;

            var icons = line.get('icons');
            icons[0].offset = (count / 2) + '%';
            line.set('icons', icons);
        }, 80);
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap"></script>
