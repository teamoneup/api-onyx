1<span class="fTab ftabCaption">
    <b class="icon-arrow-up"> History</b>
</span>

<footer style="position: fixed; ">
    <br>

    <div class="portlet box expand">
        <!-- BEGIN PAGE CONTENT INNER -->
        {!! Form::open(['url'=>'history','class' => 'form-horizontal','id'=>'historyDetail'], array('role'=>'form')) !!}

        <div class="actions" style="background-color:#FFFFFF;">
            <div class="row" style="margin-top: -11px;">
                {{--<div class="col-md-1 col-xs-2"></div>--}}
                <div class="col-md-12 col-xs-12" style="width:100%;">
                    <div class="col-md-2 col-xs-12">
                        <div class="col-md-12 col-xs-12" style=" margin-left: 47px;max-width: 92%;">
                            <div class="input-group input-group-sm select2-bootstrap-prepend">
                                <select id="select2-single-input-group-sm" name="vehicle_id"
                                        class="form-control select2">
                                    <option class="input-sm" value="Select Vehicle">Select Vehicle</option>
                                    @foreach($vehicleDetail as $detail)
                                        <option class="input-sm"
                                                value="{{$detail->vehicle_id}}">{{$detail->vehicle_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12" style="margin-left: -18px;">
                        <div class="col-md-2 col-xs-2" style="margin-top: 4px;">From
                        </div>
                        <div class="col-md-5 col-xs-6" style="min-width: 135px;">
                            <div class="input-group  date date-picker" data-date="2015-12-14"
                                 data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                <input type="text" class="form-control input-sm" readonly name="date_start">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default date-icon-size" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>
                        </div>
                        <div class="col-md-5 col-xs-4 pull-right">
                            <div class="input-icon">
                                <i class="fa fa-clock-o watch-icon"></i>
                                <input type="text" readonly="readonly"
                                       class="form-control timepicker timepicker-default input-sm time-width show-up"
                                       name="time_start">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12 pull-left" style="margin-left: -18px;">
                        <div class="col-md-2 col-xs-2 " style="margin-top: 4px;">To
                        </div>
                        <div class="col-md-5 col-xs-6">
                            <div class="input-group  date date-picker "
                                 data-date="2015-12-14 date-width" data-date-format="yyyy-mm-dd"
                                 data-date-viewmode="years">
                                <input type="text" class="form-control input-sm" readonly name="date_end">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default date-icon-size" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>

                            <!-- /input-group -->
                        </div>
                        <div class="col-md-5 col-xs-4 pull-right">
                            <div class="input-icon">
                                <i class="fa fa-clock-o watch-icon"></i>
                                <input type="text" readonly="readonly"
                                       class="form-control timepicker timepicker-default input-sm time-width show-up"
                                       name="time_end">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-xs-12">
                        <div class="col-md-4 col-xs-offset-8 pull-right col-xs-12 show-map-footer">
                            <button class="btn btn-dafault showHistory input-sm show-up pull-right" type="submit">Show
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {!! Form::close() !!}

    </div>
    <div class="row" style="position: relative; top: -10px;">
        <div class="col-md-12">
            <div class="">
                <div class="portlet-body table-responsive">
                    @if($histories == 'null')
                        <table class="table-responsive table-striped table-bordered table-hover" width="100%">
                            @else
                                <table class="table-responsive table-striped table-bordered table-hover" width="100%"
                                       id="sample_1">
                                    @endif
                                    <thead>
                                    <tr>
                                        <td>Latitude</td>
                                        <td>Longitude</td>
                                        <td>Speed</td>
                                        <td>Date</td>
                                        <td>Time</td>
                                    </tr>
                                    </thead>
                                    <tbody style="max-height: 300px !important; overflow-y: scroll"
                                           class="scroller-to-top">
                                    @if($histories == 'null')
                                        <tr class="small">
                                            <td class="imei  text-center text-danger" colspan="5">Sorry no result
                                                found !
                                            </td>
                                        </tr>
                                    @else
                                        @foreach($histories as $history)
                                            <tr>
                                                <td>{{ $history->latitude }}</td>
                                                <td>{{ $history->longitude }}</td>
                                                <td>{{ $history->speed_limit}}</td>
                                                <td>{{ $history->last_date }}</td>
                                                <td>{{ $history->last_time }}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                    @endif
                                </table>
                        </table>
                </div>
            </div>
        </div>
    </div>
</footer>