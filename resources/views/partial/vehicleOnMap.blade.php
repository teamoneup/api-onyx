<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>

{{--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>--}}


{{--script for set lat long title etc from database--}}
<script type="text/javascript">
    var markers = [
            @foreach($vehicleDetail as $detail)
                {
            "title": '{{$detail->vehicle_name}}',
            "lat": '{{$detail->current_lat}}',
            "lng": '{{$detail->current_long}}',
            "description": '{{$detail->vehicle_name}}',
            "type": '{{$detail->vehicle_status}}'
        },
        @endforeach
        ];
</script>
{{--end script for set lat long title etc from database--}}

{{--start script for set lat long for geo fancy--}}
<script type="text/javascript">



</script>

<script type="text/javascript">
    window.onload = function () {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 20,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var latlngbounds = new google.maps.LatLngBounds();
        var map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
        var i = 0;
        var interval = setInterval(function () {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var icon = "";
            switch (data.type) {
                case "disable":
                    icon = "red";
                    break;
                case "enable":
                    icon = "green";
                    break;
            }
            icon: 'http://maps.google.com/mapfiles/ms/icons/'+icon+'.png',

//            var circle = new google.maps.Circle({
//                        map: map,
//                        center: new google.maps.LatLng(28.603070,73.121913),
//                        fillColor: '#0000FF',
//                        fillOpacity : 0.6,
//                        strokeColor : '#FF0000',
//                        strokeOpacity : 0.8,
//                        strokeWeight : 2
//                    })
//                    ;
//            circle.setRadius(1.7406209616675563);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title,
//                animation: google.maps.Animation.DROP,
                icon: new google.maps.MarkerImage(icon)
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent(data.description);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
            latlngbounds.extend(marker.position);
            i++;
            if (i == markers.length) {
                clearInterval(interval);
                var bounds = new google.maps.LatLngBounds();
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);
            }
        }, 80);
    }
</script>