<div class="container">
<div class="row">
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">

            <div class="col-md-2 ">
                <div class="page-logo">
                    <a class="onyx-logo" href="{{url('/')}}">
                        <h4><b class="onyx">ONYX</b><b>GPS</b></h4>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                    <div class="hor-menu col-md-4">
                        <ul class="nav navbar-nav">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"
                                   data-hover="dropdown"
                                   data-close-others="true" aria-expanded="false">
                                    <span class="username username-hide-on-mobile"> Maps </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <input type="hidden" name="tokenForSelectdMapType" class="tokenForSelectdMapType"
                                       value="{{csrf_token()}}">
                                <ul class="dropdown-menu dropdown-menu-default" id="list">
                                    <li><a>SATELLITE</a></li>
                                    <li><a>TERRAIN</a></li>
                                    <li><a>ROADMAP</a></li>
                                    <li><a>HYBRID</a></li>
                                    <li><a>BING MAP</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="hor-menu col-md-8 pull-left">
                        {!! Form::open(['class' => 'form-horizontal']) !!}
                        <div class="col-md-8 input-sm pull-left" style="margin-left: -30px;">
                            <select name="selectedMap[]" class="bs-select input-sm form-control"  multiple="multiple">
                                @foreach($vehicleDetail as $vehicle)
                                    <option value="{{$vehicle->vehicle_id}}">{{ $vehicle->vehicle_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 input-sm pull-left" style=" margin-top: 3px;">
                            <button class="btn default form-control input-sm"  type="submit">Show</button>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li id="header_notification_bar" class=" dropdown dropdown-extended dropdown-notification">
                            <a data-close-others="true" data-hover="dropdown" class="dropdown-toggle"
                               href="{{url('/')}}">
                                <i class="icon-home home-icon"></i>
                            </a>
                            <ul></ul>
                        </li>
                        <li id="header_notification_bar" class="dropdown dropdown-extended dropdown-notification">
                            <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown"
                               class="dropdown-toggle " href="javascript:;">

                                <i class="icon-bell viewedNotification"></i>
                                <span class="badge badge-default viewedNotification setNotificationToNone"> {{auth::user()->user_notification_counter}} </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>
                                        <span class="bold "><span class="setNotificationToNone">{{auth::user()->user_notification_counter}}</span> pending</span>
                                        notifications</h3>
                                    <a href="{{url('/')}}">view all</a>
                                </li>
                                <li>
                                    <div class="slimScrollDiv"
                                         style="position: relative; overflow: hidden; width: auto; height: 250px;">
                                        <ul data-handle-color="#637283"
                                            style="height: 250px; overflow: hidden; width: auto;"
                                            class="dropdown-menu-list scroller" data-initialized="1">
                                            @foreach($NotificationsOnTOP->showNotificationOnNavbar() as $notification)
                                                <li>
                                                    <a href="javascript:;">
                                                        <span class="time">{{chop($notification->created_at->diffForHumans(),'ago')}}</span>
                                                <span class="details">
                                                    {{--<span class="label label-sm label-icon label-success">--}}
                                                    {{--<i class="fa fa-plus"></i>--}}
                                                    {{--</span>--}}
                                                    {{$notification->user_notification}}</span>
                                                    </a>
                                                </li>
                                            @endforeach

                                            {{--@endforeach--}}
                                        </ul>
                                        <div class="slimScrollBar"
                                             style="background: rgb(99, 114, 131) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail"
                                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li id="header_inbox_bar" class=" dropdown dropdown-extended dropdown-inbox">
                            <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown"
                               class="dropdown-toggle"
                               href="javascript:;">
                                <i class="icon-envelope-open"></i>
                                <span class="badge badge-default viewedNotification"> {{auth::user()->user_notification_counter}} </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>You have
                                        <span class="bold">{{auth::user()->user_notification_counter}} New</span>
                                        Messages</h3>
                                    <a href="{{url('/')}}">view all</a>
                                </li>
                                <li>
                                    <div class="slimScrollDiv"
                                         style="position: relative; overflow: hidden; width: auto; height: 275px;">
                                        <ul data-handle-color="#637283"
                                            style="height: 275px; overflow: hidden; width: auto;"
                                            class="dropdown-menu-list scroller" data-initialized="1">

                                            @foreach($NotificationsOnTOP->showNotificationOnNavbar() as $notificartion)
                                                <li>
                                                    <a href="#">
                                                <span class="subject">
                                                    <span class="from"> {{auth::user()->name}} </span>
                                                    <span class="time">{{$notificartion->created_at}}</span>
                                                </span>
                                                        <span class="message">{{$notificartion->user_notification}}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="slimScrollBar"
                                             style="background: rgb(99, 114, 131) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail"
                                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(234, 234, 234) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class=" profile-user dropdown dropdown-user pull-right">
                            <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown"
                               class="dropdown-toggle"
                               href="javascript:;">

                                <img src="{{auth::user()->user_thumbnails != ''
                                    ? auth::user()->user_thumbnails
                                    : 'images/defaultuser.png'}}" class="img-circle" alt="">

                                <span class="username username-show-on-mobile"> {!! Auth::user()->name !!} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{url('managesetting')}}">
                                        <i class=""></i> Setting </a>
                                </li>
                                <li>
                                    <a href="{{url('/')}}">
                                        <i class="icon-bell"></i> Notifications
                                        <span class="badge badge-danger"> {{auth::user()->user_notification_counter}} </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/')}}">
                                        <i class="icon-envelope-open"></i> Message
                                        <span class="badge badge-success">{{auth::user()->user_notification_counter}} </span>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{url('auth/logout')}}">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
