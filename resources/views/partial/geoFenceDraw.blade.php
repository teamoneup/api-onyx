<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        function initialize() {
            var geoFence = new Array()
            @for($i = 0 ; $i < $counter  ; $i++)
                  geoFence.push(new google.maps.LatLng(<?php printf($data[$i]); ?>));
                    @endfor
                            var mapProp = {
                center: new google.maps.LatLng(<?php printf($data[0]); ?>),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("googleMap{{$j}}"), mapProp);
            var flightPath = new google.maps.Polyline({
                path: geoFence,
                strokeColor: "#0000FF",
                strokeOpacity: 0.8,
                strokeWeight: 2
            });
            flightPath.setMap(map);
        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <script type="text/javascript">
        initialize('GeoMapInIt');
    </script>
