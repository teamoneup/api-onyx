@inject('NotificationsOnTOP','App\Notification')
@extends('layout')

@section('content')
    <div class="page-container" >
        <div class="page-content">
                <div class="container">
                    <div class="page-content-inner" style="margin-top: 15px;">
                        <div class="tiles" style="margin-right:-35px;">
                            <div class="row">

                                        <div class="col-md-2">
                                            <div class="tile bg-green">
                                                <a href="{{url('maps')}}">
                                                    <div class="tile-body">
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> Track Vehicles</div>
                                                        <div class="number"></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="tile bg-red-sunglo">
                                                <a href="{{url('managevehicle')}}">
                                                    <div class="tile-body">
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> Manage Vehicle</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-2">

                                            <div class="tile bg-blue">
                                                <a href="{{url('managegeofence')}}">
                                                    <div class="tile-body">
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> Manage Geofence</div>
                                                        <div class="number"></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    {{--</div>--}}
                                    {{--<div class="col-md-6">--}}
                                        <div class="col-md-2">
                                            <div class="tile bg-yellow">
                                                <a href="{{url('viewhistory')}}">
                                                    <div class="tile-body">
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> View History</div>
                                                        <div class="number"></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="tile bg-green">
                                                <a href="{{url('managereport')}}">
                                                    <div class="tile-body">
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> Reports</div>
                                                        <div class="number"></div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="tile bg-purple-studio ">
                                                <a href="{{url('managesetting')}}">
                                                    <div class="tile-body">
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name"> Setting</div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    {{--</div>--}}
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-6">
                            <div class="portlet light portlet-fit ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-blue"></i>
                                        <span class="caption-subject font-blue bold uppercase">Your Current Position</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="gmap_marker" class="gmaps" style="height: 340px;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="portlet light portlet-fit">
                                <div class="portlet-title tabbable-line">
                                    <div class="caption">
                                        <i class="icon-globe font-green-sharp"></i>
                                        <span class="caption-subject font-green-sharp bold uppercase">User's Notifications</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <!--BEGIN TABS-->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1_1">
                                            <div class="scroller" style="height: 339px;" data-always-visible="1"
                                                 data-rail-visible="0">
                                                @foreach($NotificationsOnTOP->showNotificationOnNavbar() as $notification)
                                                    <ul class="feeds">
                                                        <li>
                                                            <div class="col1">
                                                                <div class="cont">
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-success">
                                                                            <i class="fa fa-bell-o"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">
                                                                            {{$notification->user_notification}}

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col2">
                                                                <div class=""> {{chop($notification->created_at->diffForHumans(),'ago')}} </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                @endforeach
                                            </div>
                                        </div>

                                    </div>
                                    <!--END TABS-->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </div>
        <!-- END CONTENT -->
    </div>
    @include('partial.footer')
@stop
@section('script')

    <script src="http://www.google.com/jsapi?key=ABQIAAAAlJFc1lrstqhgTl3ZYo38bBQcfCcww1WgMTxEFsdaTsnOXOVOUhTplLhHcmgnaY0u87hQyd-n-kiOqQ"></script>
    <script>
        (function () {
            google.load("maps", "2");
            google.setOnLoadCallback(function () {
                // Create map
                var map = new google.maps.Map2(document.getElementById("gmap_marker")),
                        markerText = "<h3>Your Current Location</h3>",
                        markOutLocation = function (lat, long) {
                            var latLong = new google.maps.LatLng(lat, long),
                                    marker = new google.maps.Marker(latLong);
                            map.setCenter(latLong, 20);
                            map.addOverlay(marker);
                            marker.openInfoWindow(markerText);
                            google.maps.Event.addListener(marker, "click", function () {
                                marker.openInfoWindow(markerText);
                            });
                        };
                map.setUIToDefault();

                // Check for geolocation support
                if (navigator.geolocation) {
                    // Get current position
                    navigator.geolocation.getCurrentPosition(function (position) {
                                // Success!
                                markOutLocation(position.coords.latitude, position.coords.longitude);
                            },
                            function () {
                                // Gelocation fallback: Defaults to Stockholm, Sweden
                                markerText = "<p>Please accept geolocation for me to be able to find you. <br>I've put you in Stockholm for now.</p>";
                                markOutLocation(59.3325215, 18.0643818);
                            }
                    );
                }
                else {
                    // No geolocation fallback: Defaults to Eeaster Island, Chile
                    markerText = "<p>No location support. Try Easter Island for now. :-)</p>";
                    markOutLocation(-27.121192, -109.366424);
                }
            });
        })();
    </script>

@stop