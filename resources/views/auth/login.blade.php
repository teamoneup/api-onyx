@extends('authLayout')
@section('title')
    SignIn
@stop
@section('style')
    <link href="{{url('assets/pages/css/login.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{url('assets/pages/css/login.min.css')}}" rel="stylesheet" type="text/css"/>

@stop
@section('content')
    {{--<div class=" login">--}}
    <div class="menu-toggler sidebar-toggler"></div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{url('/')}}">
            {{--<img src="../assets/pages/img/logo-big.png" alt=""/> --}}
            <b>ONYX</b>
        </a>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
{{--        {!! Form::open(array('class' => 'login-form')) !!}--}}
        <h5 class="font-green bold">{{$success or null}}</h5>

        <h3 class="form-title font-green">Sign In</h3>

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>Enter valid email and password. </span>
        </div>
        <span id="requiredEmailPassword" class="text-danger"></span>

        {{--{{var_dump($errors->has('email'))}}--}}
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control form-control-solid placeholder-no-fix"
                   name="email" placeholder="Email" type="email" id="idEmail">
            <span id="checkEmail" class="text-danger"></span>

        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="idPassword" type="password"
                   placeholder="Password" name="password">
            <span id="checkPassword"></span>

            <div class="form-actions">
                <span class="btn green uppercase " onclick="validationOnLogin();">Login</span>
                <label class="rememberme check">&nbsp;</label>
                    {{--<input type="checkbox" name="remember" value="1"/> Remember --}}
                <a href="{{url('password/email')}}" id="forget-password" class="forget-password">Forgot Password.?</a>
            </div>
            {{--<div class="login-options">--}}
            {{--<h4>Or login with</h4>--}}
            {{--<ul class="social-icons">--}}
            {{--<li>--}}
            {{--<a class="social-icon-color facebook" data-original-title="facebook"--}}
            {{--href="{{url('login/facebooklogin')}}"></a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a class="social-icon-color twitter" data-original-title="Twitter"--}}
            {{--href="{{url('login/twitterlogin')}}"></a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a class="social-icon-color googleplus" data-original-title="Goole Plus"--}}
            {{--href="{{url('login/googlelogin')}}"></a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a class="social-icon-color github" data-original-title="GitHub"--}}
            {{--href="{{url('login/gitlogin')}}"></a>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            <div class="create-account">
                <p>
                    <a href="{{ url('auth/register') }}" class="uppercase">Create an account</a>

                    {{--<a href="{{ url('auth/register') }}" id="register-btn" class="uppercase">Create an account</a>--}}
                </p>
            </div>
        </div>

{{--        {!! Form::close() !!}--}}
                <!-- END LOGIN FORM -->
        <!-- BEGIN FORGOT PASSWORD FORM -->

        <!-- END FORGOT PASSWORD FORM -->

        <!-- END REGISTRATION FORM -->
    </div>
@stop
@section('script')
    <script>
        function validationOnLogin() {
            var password = document.getElementById('idPassword').value;
            var email = document.getElementById('idEmail').value;
//            var remember = document.getElementById('remember').value;
            if (password != '' && email != '') {
                $.getJSON('login-credential/' + password + '/' + email, function (loginValidaion) {
                   if(loginValidaion.validation == '')
                   {
                        document.location.reload();
                        document.location.reload();
                   }
                    else{
                       document.getElementById('requiredEmailPassword').innerHTML = 'Your credential dose not match with out record';

                   }
                });
            }
            else if(email == '' && password == '') {

                document.getElementById('requiredEmailPassword').innerHTML = 'Enter valid email and password.';
//              document.getElementById('checkEmail').innerHTML == 'please enter email or password';

            }
            else if(email == '') {

                document.getElementById('checkEmail').innerHTML = 'Please provide a valid email address';

            }

//    $('.clickOnLogin').click()
        }
        $(document).ready(function () {
            $('#inputPassword').keypress(function (e) {
                if (e.keyCode == 13)
                    $('.submitOnClick').click();
            });
        });
    </script>
    <script src="{{url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{url('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{url('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/pages/scripts/login.min.js')}}" type="text/javascript"></script>
@stop