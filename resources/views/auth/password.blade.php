@extends('authLayout')
@section('title')
    Forget Password
@stop
@section('style')
    <link href="{{url('assets/pages/css/login.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    {{--<div class=" login">--}}
    <div class="menu-toggler sidebar-toggler"></div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{url('/')}}">
            {{--<img src="../assets/pages/img/logo-big.png" alt=""/> --}}
            <b>ONYX</b>
        </a>
    </div>

    <div class="content">

        <!-- BEGIN REGISTRATION FORM -->
        {!! Form::open(['class' =>'login-form']) !!}
        {!! csrf_field() !!}
        <h3 class="font-green">Forget Password ?</h3>

        <p> Enter your e-mail address below to reset password. </p>

        <div class="form-group">
            {!! Form::hidden('passToken',str_random(30), array('class' => 'form-control','placeholder'=>'Enter new name')) !!}

            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email"
                   name="resetEmail" value="{{ old('resetEmail') }}"/>
            <span class="text-danger">{{$errors->has('resetEmail') ? $errors->first('resetEmail') : ''}}</span>

        </div>
        <span class="text-danger">{{$errorOnReset[0] or null}}</span>
        <div class="form-actions">
            <a href="{{url("/")}}" id="register-back-btn" class="btn btn-default">Back</a>
            <button type="submit" class="btn btn-success uppercase pull-right" >Submit</button>
        </div>
        {{--</form>--}}

        {!! Form::close() !!}
    </div>

@stop
@section('script')
    <script src="{{url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{url('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/pages/scripts/login.min.js')}}" type="text/javascript"></script>
@stop