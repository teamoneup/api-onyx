@extends('authLayout')
@section('title')
    SignIn
@stop
@section('style')
    <link href="{{url('assets/pages/css/login.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
@stop
@section('content')
    {{--<div class=" login">--}}
    <div class="menu-toggler sidebar-toggler"></div>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="{{url('/')}}">
            {{--<img src="../assets/pages/img/logo-big.png" alt=""/> --}}
            <b>ONYX</b>
        </a>
    </div>

    <div class="content">
        {{--@include('errors.list')--}}

                <!-- BEGIN REGISTRATION FORM -->
        {!! Form::open(array('class' => 'login-form')) !!}
        <h3 class="font-green">Sign Up</h3>

        <p class="hint"> Enter your personal details below: </p>

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Name</label>
            <input class="form-control placeholder-no-fix" value="{{old('name')}}" id="name" name="name" placeholder="Name" type="text">
            @if ($errors->has('name'))
                <span class="help-block small">
                        {{ $errors->first('name') }}
                    </span>
            @endif

        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">E-Mail Address</label>

            <input class="form-control placeholder-no-fix" value="{{old('email')}}" id="inputEmail" name="email" placeholder="Email" type="email">

                @if ($errors->has('email'))
                    <span class="help-block small">
                        {{ $errors->first('email') }}
                    </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('passwords') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>

            <input class="form-control placeholder-no-fix" id="inputPassword" name="passwords" placeholder="Password" type="password">
            @if ($errors->has('passwords'))
                <span class="help-block small">
                        {{ $errors->first('passwords') }}
                    </span>
            @endif
        </div>

        <div class="form-group">

            <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
            <input class="form-control placeholder-no-fix" id="inputPasswordConfirm" name="password_confirmation" placeholder="Confirm Password" type="password">

        </div>

        <div class="form-group margin-top-20 margin-bottom-20">
            <label class="check">
                <input type="checkbox" name="tnc"/> I agree to the
                <a href="javascript:;"> Terms of Service </a> &
                <a href="javascript:;"> Privacy Policy </a>
            </label>

            <div id="register_tnc_error"></div>
        </div>
        <div class="form-actions">
            <a href="{{url("/")}}" id="register-back-btn" class="btn btn-default">Back</a>
            <input type="submit" name="submit" value="Submit" class="btn btn-success uppercase pull-right submitOnClick" id="register-submit-btn">

        </div>
        {!! Form::close() !!}
    </div>

@stop
@section('script')
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--$('#inputPasswordConfirm').keypress(function (e) {--}}
                {{--if (e.keyCode == 13)--}}
                    {{--$('.submitOnClick').click();--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    <script src="{{url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{url('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/pages/scripts/login.min.js')}}" type="text/javascript"></script>
@stop