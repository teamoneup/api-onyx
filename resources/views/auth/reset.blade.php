<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<div class="container">

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2 class="text-info PageHeader">Reset Your Password</h2>
            <h4>Strong password include numbers, letters, and punctuation marks.</h4>

            {!! Form::open(['url'=>'password/reset','class' => 'form-horizontal']) !!}
            {{csrf_field()}}

            {!! Form::password('password',null, array('class' => 'form-control','placeholder'=>'Enter new password')) !!}
            {!! Form::hidden('passToken',$token, array('class' => 'form-control','placeholder'=>'Enter new name')) !!}
            <br>
            <br>
            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}

            {!! Form::close() !!}
        </div>
        <div class="col-md-3"></div>
    </div>
</div>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>