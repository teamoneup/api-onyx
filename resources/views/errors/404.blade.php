@extends('authLayout')
@section('style')
    <link href="{{url('assets/pages/css/error.min.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')

    <div class="page-inner">
        <img src="{{url('assets/pages/media/pages/earth.jpg')}}" class="img-responsive" alt=""> </div>
    <div class="container error-404">
        <h1>404 !</h1>
        <h2> Oops.. we have a problem.</h2>
        <p> Actually, the page you are looking for does not exist. </p>
        <p>
            <a href="{{url('/')}}" class="btn red btn-outline"> Return home </a>
            <br> </p>
    </div>
@stop
@section('script')
    <script src="{{url('assets/pages/scripts/ui-buttons.min.js')}}" type="text/javascript"></script>

@stop