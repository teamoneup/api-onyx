@extends('layout')
@section('style')
    <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>

    <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"
          xmlns="http://www.w3.org/1999/html"/>
    <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>

@stop
@section('content')
    <div class="container">
        <div class="page-content-inner">
            <div class="row" style="margin-top: 27px; margin-left: 0px">
                <div class="col-md-12">
                    <div class="caption font-dark">
                        <i class="fa fa-car font-dark"></i>
                        <span class="caption-subject bold uppercase">Vehicle History Details</span>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 20px; ">

                <div class="col-md-12">
                    {!! Form::open(['class' => 'form-horizontal','id'=>'historyDetail'], array('role'=>'form')) !!}
                    <div class="col-md-3">
                        <div class="input-group input-group-sm select2-bootstrap-prepend">
                            <select id="select2-single-input-group-sm" name="vehicle_id" class="form-control select2 ">
                                <option value="Select Vehicle">Select Vehicle</option>
                                @foreach($vehicleDetail as $detail)
                                    <option value="{{$detail->vehicle_id}}">{{$detail->vehicle_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-left: -15px !important;">
                        <div class="col-md-2">From</div>
                        <div class="col-md-5">
                            <div class="input-group input-medium date date-picker "
                                 data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                <input type="text" class="form-control input-sm " readonly name="date_start">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" style="height:30px;" type="button"><i
                                                class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="input-icon from-date from-date">
                                <i class="fa fa-clock-o watch-icon"></i>
                                <input type="text" class="form-control timepicker timepicker-default input-sm "
                                       readonly="readonly" name="time_start">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-left: -15px !important;">
                        <div class="col-md-2">To</div>
                        <div class="col-md-5">
                            <div class="input-group input-medium date date-picker date-from-history from-date"
                                 data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                <input type="text" class="form-control input-sm dateEnd" readonly name="date_end">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" style="height:30px;" type="button"><i
                                                        class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="input-icon"><i class="fa fa-clock-o watch-icon"></i>
                                <input type="text" class="form-control timepicker timepicker-default input-sm timeEnd"
                                       readonly="readonly" name="time_end">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 pull-right">
                        <div class="col-md-10 pull-right" style="margin-right: -10px">
                            <button class="btn btn-default showHistory input-sm pull-right" type="submit">Show</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>

            <div class="row" style="margin-top: 27px;">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">

                        </div>
                        <div class="portlet-body">
                            <table class="table-responsive table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                <tr>
                                    <th> Latitude</th>
                                    <th> Longitude</th>
                                    <th> Speed</th>
                                    <th> Date</th>
                                    <th> Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($histories == 'null')
                                    <tr class="small" style="height:53px">
                                        <td class="imei  text-center text-danger" colspan="7">Sorry no
                                            result found !
                                        </td>
                                    </tr>
                                @else
                                    @foreach($histories as $history)
                                        <tr>
                                            <td class="sorting_1"> {{ $history->latitude }}</td>
                                            <td>  {{ $history->longitude }}</td>
                                            <td> {{ $history->speed_limit}}</td>
                                            <td> {{ $history->last_date }}</td>
                                            <td> {{ $history->last_time }}</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')

    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

    <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>

    <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

    <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js"
            type="text/javascript"></script>

    <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>
    <script src="assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>

@stop