<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p> Would you like to remove this vehicle now? </p>
            </div>
            <div class="modal-footer">
                <a href="{{url('deletevehicle')}}/{{$VehicleListDetails->vehicle_id}}" class="btn green">Remove</a>
                <a href="" data-dismiss="modal" class="btn dark btn-outline">Cancel</a>
            </div>
        </div>
    </div>
</div>



<div id="geofence" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p> Would you like to remove this Geofence now? </p>
            </div>
            <div class="modal-footer">
                <a href="{{url('deletegeofence')}}/{{$VehicleListDetails->id}}" class="btn green">Remove</a>
                <a href="" data-dismiss="modal" class="btn dark btn-outline">Cancel</a>
            </div>
        </div>
    </div>
</div>
