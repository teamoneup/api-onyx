{{--***--}}
<div class="hor-menu ">
    <ul class="nav navbar-nav">
        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true" aria-expanded="false">
                <span class="username username-hide-on-mobile"> Maps </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <input type="hidden" name="tokenForSelectdMapType" class="tokenForSelectdMapType"
                   value="{{csrf_token()}}">
            <ul class="dropdown-menu dropdown-menu-default" id="list">
                <li><a>SATELLITE</a></li>
                <li><a>TERRAIN</a></li>
                <li><a>ROADMAP</a></li>
                <li><a>HYBRID</a></li>
                <li><a>BING MAP</a></li>
            </ul>
        </li>
    </ul>
</div>
<div class="hor-menu  hor-menu-light">
    {!! Form::open(['class' => 'form-horizontal']) !!}
    <div class="col-md-8 map-vehiles-lists">
        <select name="selectedMap[]" class="bs-select input-sm form-control" multiple="multiple">
            @foreach($vehicleDetail as $vehicle)
                <option value="{{$vehicle->vehicle_id}}">{{ $vehicle->vehicle_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4 map-top-btn">
        <button class="btn default form-control input-sm" type="submit">Show Vehicles</button>
    </div>
    {!! Form::close() !!}
</div>
{{--****--}}