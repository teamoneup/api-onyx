@extends('map_layout')
{{--@include('partial.map_top_navbar')--}}

@section('title')
    Home | ONYX GPS
@stop

@section('content')
    <div class="row">
        {{--<span class="selectedMapIdHome">ROADMAP</span>--}}
        <div class="col-lg-3 myrow leftpanel">
            <div class="row">
                {!! Form::open(['id'=>'filter'])!!}
                <div class="panel panel-primary mypanel">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><span class="name_color">Vehicles/Devices</span></div>

                    <!-- Table -->
                    <table class="table small">
                        <thead>
                        <tr>
                            <th class="right_border background_grey">Vehicle</th>
                            <th class="right_border background_grey">IMEI No.</th>
                            <th class="background_grey">Speed Limit</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($vehicleDetail as $vehicle)
                            <tr class="table_row_hover">
                                <td class="right_border bottom_border">{!! Form::radio('selectedId', $vehicle->vehicle_id,false, ['class' =>'vId hidden']) !!}
                                    {{ $vehicle->vehicle_name }}</td>
                                <td class="right_border bottom_border">{{ $vehicle->imei_number }}</td>
                                <td class="bottom_border">{{ $vehicle->speed_limit }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="">
                        <a href="" class="" style="line-height: 32px">&nbsp;</a>
                        <span class="editId pull-right "></span>
                        <span class="deleteId pull-right"></span>
                        <a class="fancybox fancybox.iframe pull-right btn btn-sm btn-default boxOffice"
                           href="{{url('managevehicle')}}">Add</a>
                    </div>
                </div>
                <!--panel-->
                {!! Form::close()!!}

            </div>
            <!--row- end-->
            <br>

            <div class="row command-window-panel">
                <div class="panel panel-primary mypanel"><!-- Default panel contents -->
                    <div class="panel-heading "><span class="name_color">Command Window</span></div>


                    <div class="input-group">
                    <span>
                        &nbsp;
                        <br>
                        <br>

                    </span>
                    </div>

                    <div class="input-group ommandSection ">
                        <input class="form-control" placeholder="Command" type="text">
                        <span type="submit" class="input-group-addon btn btn-default" id="basic-addon2">Send</span>
                    </div>

                    {{--{!! Form::close() !!}--}}
                    <div class="engines">
                        <br>
                        <a href="#" class="btn btn-sm btn-default ">Engine Stop</a>
                        <a href="#" class="btn btn-sm btn-default ">Engine Start</a>
                    </div>
                </div>
            </div>
            <!--row- end-->
            <br>

            <div class="row checkpoints">
                <div class="panel panel-primary mypanel">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><span class="name_color">Geo-Fence/Checkpoints</span></div>

                    <!-- Table -->
                    <table class="small table">
                        <thead>
                        <tr class="table_row_hover">
                            <th class="right_border background_grey">Name</th>
                            <th class="right_border background_grey">Time</th>
                            <th class="right_border background_grey">Days</th>
                            <th class="background_grey">Status</th>
                        </tr>
                        </thead>
                        <tbody class="geoFence">
                        {{--<span >--}}
                        {{--<tr class="geoFence">--}}
                        <td colspan="4">Please select a vehicle</td>
                        {{--</tr>--}}
                        {{--</tr>--}}
                        {{-- // data foundData from onix.js--}}

                        {{--</span>--}}

                        </tbody>
                    </table>

                    <div class="">

                        <a href="{{url('managegeofence')}}"
                           class="fancybox fancybox.iframe pull-right btn btn-sm btn-default">Add</a>
                        <a href="" class="" style="line-height: 32px">&nbsp;</a>
                    </div>
                </div>
                <!--row- end-->
            </div>
            <br>

            <div class="row history">

                <div class="panel panel-primary small">
                    <!-- Default panel contents -->
                    <div class="panel-heading mypanel"><span class="name_color">History</span></div>
                    <input type="hidden" class="token" value="{{csrf_token()}}">
                    {{-- // data foundData from onix.js--}}
                    <h5 class="foundData">No vehicle selected</h5>

                </div>
                <!--panel-->
                {{--</form>--}}
            </div>
            <!--row- end-->
        </div>
        <!--col-lg-3-->
        <div class="col-lg-9 myrow bingmappanel">

            <div id="map_canvas" class="map_home"></div>
        </div>
        <!--row- end-->
    </div>
@stop

@section('script')
    @if($vehicleDetail->count() == 0)
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
        <script type="text/javascript">
            function GetMap() {

                var map = new Microsoft.Maps.Map(document.getElementById("map_canvas"),
                        {
                            credentials: "Ass6i-oNGFVBxfBX2M7Kt7gZE_X2wol_eluFeXoTNOZsmowvFPN_VeyQfAl5xSWZ",
                            center: new Microsoft.Maps.Location(28.57728604839182, 73.54522705078125),
                            mapTypeId: Microsoft.Maps.MapTypeId.road,
                            zoom: 7
                        });
            }
        </script>
        {{--@elseif($vehicleDetail->current_lat = 0 && $vehicleDetail = 0)--}}
        {{--{--}}
        {{----}}
        {{--}--}}
    @else
        <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>

        <script type="text/javascript">
            function GetMap() {
                var map = new Microsoft.Maps.Map(document.getElementById("map_canvas"),
                        {
                            credentials: "Ass6i-oNGFVBxfBX2M7Kt7gZE_X2wol_eluFeXoTNOZsmowvFPN_VeyQfAl5xSWZ",
                            center: new Microsoft.Maps.Location(20.593684, 78.962880),
                            mapTypeId: Microsoft.Maps.MapTypeId.road,
                            zoom: 5
                        });

                dataLayer = new Microsoft.Maps.EntityCollection();
                map.entities.push(dataLayer);

                var infoboxLayer = new Microsoft.Maps.EntityCollection();
                map.entities.push(infoboxLayer);

                infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), {
                    visible: false,
                    offset: new Microsoft.Maps.Point(0, 20)
                });
                infoboxLayer.push(infobox);
                AddData();

                function AddData()
                {
                        @foreach($vehicleDetail as $detail)
                        {
                        var pin1 = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(+'{{$detail->current_lat}}','{{$detail->current_long}}'));
                        pin1.Title = "Vehicle Name: {{$detail->vehicle_name}}";
                        pin1.Description = "IMEI Number : {{$detail->imei_number}}";
                        Microsoft.Maps.Events.addHandler(pin1, 'click',displayInfobox);
                        dataLayer.push(pin1);
                    }
                    @endforeach
                }
                function displayInfobox(e) {
                    if (e.targetType == 'pushpin') {
                        infobox.setLocation(e.target.getLocation());
                        infobox.setOptions({visible: true, title: e.target.Title, description: e.target.Description});
                    }
                }
            }
        </script>
    @endif
@stop