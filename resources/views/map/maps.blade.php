@extends('map_layout')

@section('title')
    Map view
@stop

@section('style')
    <style>
        .page-header.navbar .hor-menu .navbar-nav > li .dropdown-menu li > a, .page-header.navbar .hor-menu .navbar-nav > li .dropdown-menu li > a > i
        {
            background-color: white !important;
            color: black !important;
        }
        .btn.dropdown-toggle.btn-default {
            background-color: transparent !important;
            color: #c6cfda;
            border: none;
        }
        .btn-group.bootstrap-select.show-tick.bs-select.input-sm.form-control {
            background-color: transparent !important;
        }
        .btn.default.form-control.input-sm
        {
            /*border: 1px solid #2B3643 !important;*/
            padding: 0px 6px !important;
            border-radius: 5px !important;
            font-weight: bold !important;
            color: black !important;
        }
        div.dataTables_scrollBody {
            background: none!important;
        }

    </style>
    <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"
          xmlns="http://www.w3.org/1999/html"/>
    <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        #sample_1_length, #sample_1_filter, .dt-buttons {
            display: none;
        }

        html, body {
            overflow: hidden;
        }
    </style>

    <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>

    <!-- END PAGE LEVEL PLUGINS -->
@stop

@section('content')
    <div class="row" style="margin-top: 45px;">
    @if($vehicleDetail->count() > 0 && $mapTypeId =="BING MAP")
            <div class="portlet-body ">
                <div id="bing_Map" class=""></div>
            </div>
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"></script>
            <script type="text/javascript">
                //                    initialize('bingMap');

                //                    function initialize() {
                var map = new Microsoft.Maps.Map(document.getElementById("bing_Map"),
                        {
                            credentials: "Ass6i-oNGFVBxfBX2M7Kt7gZE_X2wol_eluFeXoTNOZsmowvFPN_VeyQfAl5xSWZ",
                            center: new Microsoft.Maps.Location(28.01035833, 73.33397667),
                            mapTypeId: Microsoft.Maps.MapTypeId.road,
                            zoom: 8
                        });

                dataLayer = new Microsoft.Maps.EntityCollection();
                map.entities.push(dataLayer);

                var infoboxLayer = new Microsoft.Maps.EntityCollection();
                map.entities.push(infoboxLayer);

                infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), {
                    visible: false,
                    offset: new Microsoft.Maps.Point(0, 20)
                });
                infoboxLayer.push(infobox);
                AddData();

                function AddData() {
                        @foreach($vehicleDetail as $detail)
                        {
                        var pin1 = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(+'{{$detail->current_lat}}', '{{$detail->current_long}}'));
                        pin1.Title = "Vehicle Name: {{$detail->vehicle_name}}";
                        pin1.Description = "IMEI Number : {{$detail->imei_number}}";
                        Microsoft.Maps.Events.addHandler(pin1, 'click', displayInfobox);
                        dataLayer.push(pin1);
                    }
                    @endforeach
//                                                        window.location.reload("maps")

                }
                function displayInfobox(e) {
                    if (e.targetType == 'pushpin') {
                        infobox.setLocation(e.target.getLocation());
                        infobox.setOptions({visible: true, title: e.target.Title, description: e.target.Description});
                    }
                }
                //                    }
            </script>
        @elseif($vehicleDetail->count() > 0)
            <div>
                <!-- BEGIN MARKERS PORTLET-->
                <div class="portlet-body">
                    <div id="gmap_marker" class="gmaps"></div>
                </div>
                <!-- END MARKERS PORTLET-->
            </div>
            <div>
                <!-- BEGIN BASIC PORTLET-->
                <div class="portlet-body">
                    <div id="gmap_basic" class="gmaps"></div>
                </div>
                <!-- END BASIC PORTLET-->
            </div>
        @else
            <div>
                <!-- BEGIN BASIC PORTLET-->
                <div class="portlet-body">
                    <div id="gmap_basic" class="gmaps"></div>
                </div>
                <!-- END BASIC PORTLET-->
            </div>
        @endif
        @include('partial.slideupFooter')

    </div>
    <!-- END CONTENT -->
@stop

@section('script')

    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>

    <script src="assets/pages/scripts/ui-buttons.min.js" type="text/javascript"></script>

    {{--datatable start--}}
    <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>

    {{--datatable end--}}
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    @include('partial.history')
    <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js"
            type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/table-datatables-scroller.min.js" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    @include('map.mapChanger')
    {{--<script id="result" src="assets/global/plugins/gmaps/gmaps.js" type="text/javascript"></script>--}}
    <script src="assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
            type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    {{--<script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>--}}
    <script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>

    <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js"
            type="text/javascript"></script>
    <script src="assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

    {{--<script src="assets/pages/scripts/maps-google.min.js" type="text/javascript"></script>--}}
    @include('map.gmaps')

@stop