@extends('layout')
@section('title')
    |Setting|
@stop
@section('style')

    <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>

@stop
@section('content')
    <div class="container-fluid" style="margin-top: 63px;">
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="portlet box green">
                            <div class="portlet-title ">
                                <div class="caption profile-caption-height">
                                    <span class="fa fa-user"></span> User profile
                                    <br>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                {!! Form::open(['url'=>'managesetting','class' => 'form-horizontal','accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data']) !!}
                                <div class="form-body">
                                    <div class="portlet light profile-sidebar-portlet">
                                        <div class="profile-sidebar">
                                            <div class="portlet light profile-sidebar-portlet ">
                                                <div class="profile-userpic img-responsive">
                                                    {{--<img src="{{$profilePicture or 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image'}}"--}}
                                                    {{--class="img-thumbnail img-circle" alt="">--}}
                                                    @if($profilePicture == '')
                                                        <img src="images/defaultuser.png"
                                                             class="img-thumbnail img-circle img-responsive" alt="">
                                                    @else
                                                        <img src="{{$profilePicture}}"
                                                             class="img-thumbnail img-circle img-responsive" alt="">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div data-provides="fileinput" class="fileinput fileinput-new">
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="hidden" value="" name="...">
                                                    <input type="file" name="image"> </span>
                                                <a data-dismiss="fileinput" class="btn red fileinput-exists"
                                                   href="javascript:;">Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">

                                        <div class="col-md-12">
                                            {!! Form::text('name',auth::user()->name, array('class' => 'form-control input-circle','placeholder'=>'User Name')) !!}

                                            @if ($errors->has('name'))
                                                <span class="help-block small">
                                                   &nbsp;&nbsp; {{ $errors->first('name') }}
                                                 </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-12">
                                            {!! Form::text('address',auth::user()->address, array('class' => 'form-control input-circle','placeholder'=>'Permanent Address')) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="input-group">
                                    <span class="input-group-addon input-circle-left">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                                {!! Form::text('email',auth::user()->email, array('class' => 'form-control input-circle-right','placeholder'=>'e.g. example@gmail.com','readonly')) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('mobile') ? 'has-error' : ''}}">

                                        <div class="col-md-12">
                                            {!! Form::text('mobile',auth::user()->mobile, array('class' => 'form-control input-circle','placeholder'=>'e.g. 9876543210')) !!}

                                            @if ($errors->has('mobile'))
                                                <span class="help-block small">
                                                   &nbsp;&nbsp; {{ $errors->first('mobile') }}
                                                 </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-12 pul-right">Member Since : {{$member_since[0]}}
                                            {{--                                            {!! Form::text('created_at',$member_since[0], array('class' => 'form-control input-circle','readonly')) !!}--}}
                                        </div>
                                    </div>

                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-12">
                                            <button class="btn btn-circle green" type="submit">Save Changes</button>
                                            <button class="btn btn-circle grey-salsa btn-outline" type="button">Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                {{--                                @include('errors.list')--}}
                                        <!-- END FORM-->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-car"></i>Vehicle Subscription Details
                                </div>
                                <div class="tools"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 10px"><b> # </b></th>
                                            <th><b>Name </b></th>
                                            <th><b>Register No.</b></th>
                                            <th><b>IMEI </b></th>
                                            <th><b>SIM No </b></th>
                                            <th><b>Plan </b></th>
                                            <th><b> Action </b></th>
                                        </tr>
                                        </thead>
                                        <?php $i = 1;?>
                                        @foreach($VehicleList as $VehicleListDetails)
                                            <tbody>
                                            <tr>
                                                <td style="width: 10px;"> <?php echo $i++; ?> </td>
                                                <td> {{$VehicleListDetails->vehicle_name}}</td>
                                                <td> {{$VehicleListDetails->registration_number}}</td>
                                                <td> {{$VehicleListDetails->imei_number}}</td>
                                                <td> {{$VehicleListDetails->sim_number}}</td>
                                                <td> ---</td>
                                                <td>
                                                    <a data-toggle="modal" class="btn green sbold"
                                                       href="{{url('managevehicle')}}">VIEW</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partial.footer')
@stop
@section('script')

    <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
            type="text/javascript"></script>

    <script src="assets/pages/scripts/form-samples.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

    <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>


@stop