@extends('edit_layout')
@include('partial.OldTopNavbar')

@section('title')
    {{ $vehicleName }} | History Log
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <ul class=" bg-primary">
                <li class="active list-unstyled latBoxHeight-34"><h4 class="afterMenuText"><a href="{{url('/')}}"> <span class="name_color">Home</span></a> <span class="name_color"> >>
                History Log of <b class="text-uppercase">{{ $vehicleName }}</b></span></h4></li>
            </ul>
            {!! Form::open() !!}
            <div class="form-group">
                <div class="col-md-6">
                    <p class="SearchByDateFromTO">

                        <sapn>From  <input type="text" class="date start " name="date_start"></sapn>
                        <span> <input type="text" class="time start ui-timepicker-input" name="time_start" autocomplete="off"> </span>
                        <span>To  <input type="text" class="time end ui-timepicker-input" name="time_end" autocomplete="off"></span>
                        <span> <input type="text" class="date end" name="date_end"></span>
                    </p>

                </div>
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-default col-lg-12" name="history" value="tableHistory">Find History</button>

                    {{--<button type="submit" class="btn btn-default col-sm-12">View History</button>--}}
                </div>
                <div class="col-lg-2">
                    <button type="submit" class="btn btn-default col-lg-12" name="history" value="viewOnMap">View On Map</button>

                </div>
                <div class="col-lg-1">
                    <a href=""> <button type="reset" class="btn btn-default col-sm-12">Clear</button></a>
                </div>
                <div class="col-lg-1">
                    <button onclick="window.print();" type="button" class=""><i class="fa fa-print" style="font-size: 30px; color: black;"></i></button>
                </div>
            </div>
            <br>
            <hr>
            {!! Form::hidden('vehicleId', $vehicleId) !!}
            {!! Form::close()!!}
                    <!-- Table -->
            <table class="table" id="historyTable">
                <thead>
                <tr>
                    <th>IMEI Number</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Speed Limit</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Bearing</th>
                </tr>
                </thead>
                <tbody>

                @foreach($histories as $history)
                    <tr>
                        <td>{{ $history->imei_number }}</td>
                        <td>{{ $history->latitude }}</td>
                        <td>{{ $history->longitude }}</td>
                        <td>{{ $history->speed_limit}}</td>
                        <td>{{ $history->last_date }}</td>
                        <td>{{ $history->last_time }}</td>
                        <td>{{ $history->vehicle_bearing}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
    {!! Html::script('js/jquery.dataTables.min.js') !!}

    <script>
        $(document).ready(function() {
            $('#historyTable').dataTable( {
                "order": [[ 3, "desc" ]]
            } );
        } );

        //    $(document).ready(function(){
        //    $('#historyTable').DataTable();
        //    });
    </script>
@stop