@extends('layout')
@section('style')

@stop
@section('content')
        <!-- BEGIN CONTAINER -->
<div class="page-container">
    <div class="page-content">
        <div class="page-content">
            <div class="container">
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup"
                                              data-value="{{count($VehicleCounter)}}">{{count($VehicleCounter)}}</span>
                                    </div>
                                    <div class="desc">Number of Vehicle(s)</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <b></b>
                                </div>
                                <div class="details">
                                    <div class="number">
                                            <span data-counter="counterup"
                                                  data-value="{{count($counterForLocationUpdate)}}">{{count($counterForLocationUpdate)}}</span>
                                    </div>
                                    <div class="desc"> Total Location Update</div>
                                </div>

                            </div>
                        </div>
                        <?php $lat = ''; $lng = ''; ?>
                        @foreach($VehicleList as $VehicleListDetails)
                            <span class="hidden">{{ $lat = $VehicleListDetails->current_lat}}</span>
                            <span class="hidden">{{ $lng = $VehicleListDetails->current_long}}</span>
                        @endforeach
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                            <span class="small" data-counter="counterup"
                                                  data-value="{{round($lat,4) }}"></span> ,
                                                <span class="small" data-counter="counterup"
                                                      data-value="{{round($lng,4) }}">
                                                </span>
                                    </div>
                                    <div class="desc">Last Updated Position</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a href="{{url('addvehicle')}}">
                                <div class="dashboard-stat blue">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>

                                    <a href="{{url('addvehicle')}}">
                                        <div class="details">
                                            <div class="number">+
                                                <span data-counter="counterup"></span>
                                            </div>
                                            <div class="desc">Add Vehicle</div>
                                        </div>
                                    </a>
                                </div>
                            </a>
                        </div>
                    </div>


                    <!-- Chart started -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-car"></i>Manage Vehicle
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th style="width: auto;"> <b>#</b></th>
                                                <th> <b>Name</b></th>
                                                <th> <b>Registration No.</b></th>
                                                <th> <b>IMEI</b></th>
                                                <th> <b>SIM No</b></th>
                                                <th> <b>Geofence</b></th>
                                                <th class="text-center"> <b>Action</b></th>
                                            </tr>
                                            </thead>
                                            @foreach($VehicleList as $VehicleListDetails)
                                            <tbody>
                                            <?php static $i = 1;?>
                                            <tr>
                                                <td style="width: auto;" > <?php echo $i++; ?> </td>
                                                <td class=""> {{$VehicleListDetails->vehicle_name}}</td>
                                                <td class=""> {{$VehicleListDetails->registration_number}}</td>
                                                <td class=""> {{$VehicleListDetails->imei_number}}</td>
                                                <td class=""> {{$VehicleListDetails->sim_number}}</td>
                                                <td class="">
                                                    <a class="btn green" href="{{url('addgeofence')}}">ADD</a>
                                                </td>

                                                <td>
                                                    <a class="btn green sbold" href="{{url('managevehicle')}}/{{$VehicleListDetails->vehicle_id}}">EDIT</a>
                                                    <a class=" btn red sbold" data-toggle="modal" href="#static">REMOVE</a>
                                                    @include('history.modal_confirmation')
                                                </td>
                                            </tr>
                                            </tbody>
                                                @endforeach
                                        </table>
                                        {!! $VehicleList->render() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partial.footer')
</div>
@stop
@section('script')

    <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>


    {{--Modals--}}
    <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>

    {{--Modals end--}}


@stop