<div class="form-group">
    <span class="col-lg-3 control-label"> Vehicle Name <i class="text-danger">*</i></span>
    <div class="col-lg-9">
        {!! Form::text('vehicle_name',null, array('class' => 'form-control','placeholder'=>'Vehicle Name')) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('registration_number','Vehicle Registration Number', array('class' => 'col-lg-3 control-label')) !!}
    <div class="col-lg-9">
        {!! Form::text('registration_number',null, array('class' => 'form-control','placeholder'=>'Registration Number')) !!}
    </div>
</div>

<div class="form-group">
    <span class="col-lg-3 control-label"> IMEI Number <i class="text-danger">*</i></span>
    <div class="col-lg-9">
        {!! Form::text('imei_number',null, array('class' => 'form-control','placeholder'=>'IEMI Number')) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('sim_number','SIM Mobile Number', array('class' => 'col-lg-3 control-label')) !!}
    <div class="col-lg-9">
        {!! Form::text('sim_number',null, array('class' => 'form-control','placeholder'=>'SIM Number')) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('speed_limit','Speed Limit', array('class' => 'col-lg-3 control-label')) !!}
    <div class="col-lg-9">
        {!! Form::text('speed_limit',null, array('class' => 'form-control','placeholder'=>'Speed Limit')) !!}
    </div>
</div>
<script>


    var z = document.getElementById("notSupport");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition);
        } else {
            z.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        document.getElementById("latVehicle").value = position.coords.latitude;
        document.getElementById("longVehicle").value = position.coords.longitude;
    }
</script>




