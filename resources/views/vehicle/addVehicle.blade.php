@extends('layout')

@section('title')
    |Add Vehicle|
    @stop
    @section('style')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@stop

@section('content')

        {{--<div class="page-content">--}}
            <div class="container">

                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light " id="form_wizard_1">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase"> Simple Steps for Add Vehicle -
                                                <span class="step-title"> Step 1 of 5 </span>
                                            </span>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    {!! Form::open(['url'=>'managevehicle','class' => 'form-horizontal formData','id'=>'submit_form'], array('role'=>'form')) !!}
                                    <div class="form-wizard">
                                        <div class="form-body">
                                            <ul class="nav nav-pills nav-justified steps">
                                                <li>
                                                    <a href="#tab1" data-toggle="tab" class="step">
                                                        <span class="number"> 1 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> Name </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab2" data-toggle="tab" class="step">
                                                        <span class="number"> 2 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i>Registration </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab3" data-toggle="tab" class="step active">
                                                        <span class="number"> 3 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> IMEI </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab4" data-toggle="tab" class="step active">
                                                        <span class="number"> 4 </span>
                                                                    <span class="desc">
                                                                        <i class="fa fa-check"></i> Mobile </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#tab5" data-toggle="tab" class="step active">
                                                        <span class="number"> 5 </span>
                                                                    <span class="desc">
                                                         <i class="fa fa-check"></i> Speed</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div id="bar" class="progress progress-striped" role="progressbar">
                                                <div class="progress-bar progress-bar-success"></div>
                                            </div>
                                            <div class="tab-content">

                                                <div class="tab-pane active" id="tab1">
                                                    <h3 class="block">Provide your Vehicle Name </h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Vehicle Name
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            {!! Form::text('vehicle_name',null, array('class' => 'form-control','placeholder'=>'Vehicle Name', 'required')) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab2">
                                                    <h3 class="block">Provide your Registration Number</h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Registration Number
                                                        </label>

                                                        <div class="col-md-4">
                                                            {!! Form::text('registration_number',null, array('class' => 'form-control','placeholder'=>'Registration Number')) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab3">
                                                    <h3 class="block">Provide your IMEI details</h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">IMEI Number
                                                            <span class="required"> * </span>
                                                        </label>

                                                        <div class="col-md-4">
                                                            {!! Form::text('imei_number',null, array('class' => 'form-control','placeholder'=>'IEMI Number', 'required', 'pattern'=>'([0-9]){15,}','title'=>'Min. 15 Number Required')) !!}
                                                            <span class="help-block"> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab4">
                                                    <h3 class="block">Provide your Mobile Number</h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Mobile Number
                                                        </label>

                                                        <div class="col-md-4">
                                                            {!! Form::text('sim_number',null, array('class' => 'form-control','placeholder'=>'SIM Number')) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab5">
                                                    <h3 class="block">Provide your Vehicle Speed</h3>

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Speed Limit
                                                        </label>

                                                        <div class="col-md-4">
                                                            {!! Form::text('speed_limit',null, array('class' => 'form-control','placeholder'=>'Speed Limit')) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <a href="javascript:;" class="btn default button-previous">
                                                        <i class="fa fa-angle-left"></i> Back </a>
                                                    <a href="javascript:;" class="btn btn-outline green button-next" >
                                                        Continue
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                    {!! Form::submit('Add Vehicle', ['class' => 'btn btn-primary button-submit','onclick'=>'vechicleDetailes();']) !!}
                                                    {{--<input type="button" class="btn btn-primary" name="button" value="Add Vehicle" onclick="vechicleDetailes();">--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--</form>--}}
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        {{--</div>--}}

    @include('partial.footer')
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
    {{--</body>--}}
    @stop
    @section('script')
            <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="assets/pages/scripts/form-wizard.min.js" type="text/javascript"></script>

    <script>
        function vechicleDetailes() {
            $("form#submit_form").submit(function () {
                $.ajax({
                    url: 'managevehicle',
                    type: 'POST',
                    cache: false,
                    dataType: 'json',
                    data: $('form#submit_form').serialize(),
                    beforeSend: function () {
                        window.location.assign("managevehicle")
                    },
                });

            });
        }
    </script>
@stop
