@extends('edit_layout')
@section('title')
    |Edit Vehicle|
@stop
@section('content')
    <div class="container">
        <div class="row" style="height: 387px;">
            <div class="col-lg-3"></div>

            <div class="col-md-6">
                {!! Form::model($editVehicle,['method' => 'post','class' => 'form-horizontal','id'=>'myForm' ]) !!}
                @include('vehicle.vehicleForm',['editType'=>'readonly'])
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-9">
                        {!! Form::submit('Update Vehicle', ['class' => 'btn btn-info', 'id'=>'goHome']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
                @include('errors.list')
            </div>
        </div>
    </div>
    @include('partial.footer')
@stop

@section('script')


@stop