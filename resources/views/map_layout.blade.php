    @inject('NotificationsOnTOP','App\Notification')
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    {{--<link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>--}}
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
    {{--theme one--}}
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout/css/custom.css" rel="stylesheet" type="text/css">
    <link href="assets/custom.css" rel="stylesheet" type="text/css">
    {{--end theme one--}}
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    {{--<link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>--}}
    {{--<link href="assets/layouts/layout/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color"/>--}}
    {{--<link href="assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>--}}
    <link href="assets/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME LAYOUT STYLES -->
    @yield('style')
            <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->

<body>
<div>
    @include('partial.map_top_navbar')
    @yield('content')
</div>
</body>
<div>
    @yield('footer')
</div>

<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
<script src="assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
<script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

<script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

{{--//this script for track vehicle footer toggle--}}
<script>
    jQuery(function($){
        var clicks = 0;
        $('.fTab').on('click', function(){
            $(this).toggleClass('active');
            if(clicks%2 == 0)
            $('.fTab').html('<b class="icon-arrow-down "style="z-index: 10001"> History</b>');
            else
            $('.fTab').html('<b class="icon-arrow-up"> History</b>');
            clicks +=1;
        });
    })

</script>
<script>
    $('.viewedNotification').on('click', function () {
        $.ajax({
            method: "GET",
            url: "updatenotificationtoviewed",
            beforeSend: function () {

                $(".setNotificationToNone").html("0");

            }
        });
    });

</script>
{{--<script>--}}
    {{--$(function () {--}}
        {{--$('#list li').click(function () {--}}
            {{--var token = $(".tokenForSelectdMapType").val();--}}

            {{--setmapTypeId = $(this).text();--}}
            {{--console.log(setmapTypeId);--}}
            {{--$.ajax({--}}
                {{--method: "POST",--}}
                {{--url: "maptype/" + setmapTypeId,--}}

                {{--data: {product: setmapTypeId, _token: token},--}}
                {{--beforeSend: function () {--}}
{{--//                                window.location.reload("maps")--}}
                {{--},--}}
                {{--success: function () {--}}
                    {{--window.location.reload("maps")--}}
                {{--}--}}

            {{--});--}}
            {{--return false;--}}
        {{--});--}}
    {{--});--}}

{{--</script>--}}

@yield('script')

@include('flash')
</html>
