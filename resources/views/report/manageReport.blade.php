@inject('NotificationsOnTOP','App\Notification')
@extends('layout')
@section('style')
    {{--<link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet"--}}
    {{--type="text/css"/>--}}
    {{--<link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>--}}

    {{--<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"--}}
    {{--type="text/css"/>--}}
    {{--<link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css"/>--}}
    {{--<link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>--}}
    <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
@stop
@section('content')

    <body class="page-container-bg-solid page-boxed">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container">
                    <!-- BEGIN PAGE BREADCRUMBS -->

                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">

                    </div>
                    <!-- END PAGE CONTENT INNER -->
                    <!-- Chart started -->

                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN BASIC PORTLET-->
                            <a href="{{url('maps')}}">
                                <div class="portlet light portlet-fit ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase">Track Vehicles</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="gmap_basic" class="gmaps"></div>
                                    </div>
                                </div>
                            </a>
                            <!-- END BASIC PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light map-height ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">Site Visits</span>
                                        <span class="caption-helper">weekly stats...</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="site_statistics_loading" style="display: none;">
                                        <img alt="loading" src="assets/global/img/loading.gif"></div>
                                    <div class="display-none" id="site_statistics_content" style="display: block;">
                                        <div class="chart" id="site_statistics"
                                             style="padding: 0px; position: relative;">
                                            <canvas class="flot-base"
                                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 515px; height: 300px;"
                                                    width="515" height="300"></canvas>
                                            <div class="flot-text"
                                                 style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                                <div class="flot-x-axis flot-x1-axis xAxis x1Axis"
                                                     style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 11px; text-align: center;">
                                                        02/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 69px; text-align: center;">
                                                        03/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 127px; text-align: center;">
                                                        04/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 185px; text-align: center;">
                                                        05/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 243px; text-align: center;">
                                                        06/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 301px; text-align: center;">
                                                        07/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 359px; text-align: center;">
                                                        08/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 417px; text-align: center;">
                                                        09/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 476px; text-align: center;">
                                                        10/2013
                                                    </div>
                                                </div>
                                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis"
                                                     style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div style="position: absolute; top: 273px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 19px; text-align: right;">
                                                        0
                                                    </div>
                                                    <div style="position: absolute; top: 220px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        1000
                                                    </div>
                                                    <div style="position: absolute; top: 166px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        2000
                                                    </div>
                                                    <div style="position: absolute; top: 113px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        3000
                                                    </div>
                                                    <div style="position: absolute; top: 59px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        4000
                                                    </div>
                                                    <div style="position: absolute; top: 6px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        5000
                                                    </div>
                                                </div>
                                            </div>
                                            <canvas class="flot-overlay"
                                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 515px; height: 300px;"
                                                    width="515" height="300"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                    <!-- Chart end -->
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN BASIC PORTLET-->
                            <a href="{{url('maps')}}">
                                <div class="portlet light portlet-fit ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class=" icon-layers font-red"></i>
                                            <span class="caption-subject font-red bold uppercase">Track Vehicles</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="gmap_basic" class="gmaps"></div>
                                    </div>
                                </div>
                            </a>
                            <!-- END BASIC PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light map-height ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">Site Visits</span>
                                        <span class="caption-helper">weekly stats...</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="site_statistics_loading" style="display: none;">
                                        <img alt="loading" src="assets/global/img/loading.gif"></div>
                                    <div class="display-none" id="site_statistics_content" style="display: block;">
                                        <div class="chart" id="site_statistics"
                                             style="padding: 0px; position: relative;">
                                            <canvas class="flot-base"
                                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 515px; height: 300px;"
                                                    width="515" height="300"></canvas>
                                            <div class="flot-text"
                                                 style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">
                                                <div class="flot-x-axis flot-x1-axis xAxis x1Axis"
                                                     style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 11px; text-align: center;">
                                                        02/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 69px; text-align: center;">
                                                        03/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 127px; text-align: center;">
                                                        04/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 185px; text-align: center;">
                                                        05/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 243px; text-align: center;">
                                                        06/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 301px; text-align: center;">
                                                        07/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 359px; text-align: center;">
                                                        08/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 417px; text-align: center;">
                                                        09/2013
                                                    </div>
                                                    <div style="position: absolute; max-width: 57px; top: 285px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 476px; text-align: center;">
                                                        10/2013
                                                    </div>
                                                </div>
                                                <div class="flot-y-axis flot-y1-axis yAxis y1Axis"
                                                     style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;">
                                                    <div style="position: absolute; top: 273px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 19px; text-align: right;">
                                                        0
                                                    </div>
                                                    <div style="position: absolute; top: 220px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        1000
                                                    </div>
                                                    <div style="position: absolute; top: 166px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        2000
                                                    </div>
                                                    <div style="position: absolute; top: 113px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        3000
                                                    </div>
                                                    <div style="position: absolute; top: 59px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        4000
                                                    </div>
                                                    <div style="position: absolute; top: 6px; font: small-caps 400 11px/14px &quot;Open Sans&quot;,sans-serif; color: rgb(111, 123, 138); left: 1px; text-align: right;">
                                                        5000
                                                    </div>
                                                </div>
                                            </div>
                                            <canvas class="flot-overlay"
                                                    style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 515px; height: 300px;"
                                                    width="515" height="300"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    @include('partial.footer')
    @stop
    @section('script')
        {{--<script src="assets/pages/scripts/ui-extended-modals.min.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"--}}
        {{--type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>--}}

        {{--<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>--}}

        {{--<script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>--}}
        <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        {{--<script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>--}}
        <script src="assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        {{--<script src="assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>--}}
        <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        {{--<script src="assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>--}}
        {{--<script src="assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>--}}

        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script src="assets/global/plugins/gmaps/gmaps.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/maps-google.min.js" type="text/javascript"></script>

@stop