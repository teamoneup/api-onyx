$(function () {

    $('.table_row_hover').click(function () {
        var d = $(this).find('input[type=radio]').prop('checked', true);
        //alert(d)
        //})
        $(this).addClass('table_row_active').siblings().removeClass('table_row_active');

        var vId = '';
        var editId = '';
        //var editGeoFenceId = '';
        var geoFence = '';
        var delId = '';
        //var deleteGeoFenceId = '';
        var radio_value = "Search Result for ";
        //$(":radio").each(function () {
        //    var ischecked = $(this).is(":checked");
        //    if (ischecked) {
        //        radio_value += $(this).val() + " | ";
        //    }
        //});
        var token;
        token = $(".token").val();
        $(".idDetails").html(radio_value);
        $.post("", $("#filter").serialize(), function (data) {
            $.each(data, function () {
                if (this.current_lat != '') {

                    if ((this.landmark_name != undefined) == true) {
                        geoFence += '<tr class="table_row_geofence_hover">'+
                            '<td class="small right_border bottom_border">' + this.landmark_name + '</td>' +
                            '<td class="small right_border bottom_border">' + this.time_from + '-' + this.time_to + '</td>' +
                            '<td class="small right_border bottom_border">' + this.days + '</td>' +
                            '<td class="small right_border bottom_border">' +
                            '<div >' +
                            '<div >' +
                            this.status +
                            '</div>' +
                            '<div >' +
                            '<a class="fancybox fancybox.iframe editFancy editShow" href="editgeofence/' + this.id + '"> <span class="glyphicon glyphicon-edit"></span></a>' +
                            '&nbsp;&nbsp;&nbsp;<a class="editFancy editShow" href="deletegeofence/' + this.id + '"> <span class="glyphicon glyphicon-remove"></span></a>' +
                                //'<a class="editFancy editShow" href="showgeofenceonhome/' + this.id + '"> <span class="glyphicon glyphicon-arrow-right"></span></a>' +
                            '</div>' +
                            '</div>' +
                            '</td>' +
                            '</tr>';

                        geoFence += '<br>';
                    }
                    else {
                        geoFence += '<tr class="small"><td colspan="3"> &nbsp; ! Sorry no geofance found</td></tr>';
                    }
                    editId = '<a class="fancybox fancybox.iframe btn btn-sm btn-default editFancy" href="managevehicle/' + this.vehicle_id + '">Edit</a>';
                    delId = '<a class="btn btn-sm btn-default delFancy" href="deletevehicle/' + this.vehicle_id + '">Delete</a>';

                    //editGeoFenceId = '<a class="fancybox fancybox.iframe btn btn-default editFancy" href="editgeofence/' + this.id + '">Edit</a>';
                    //deleteGeoFenceId = '<a class="btn btn-default delFancy" href="deletevehicle/' + this.id + '">Delete</a>';

                    vId = '<form method="post" action="history/' + this.vehicle_id + '">' +
                        '<input type="hidden" name="_token" value="' + token + '">' +
                        '<button class="btn btn-default" name="history" value="tableHistory">View as Data Log</button>' +
                        '<button  class="btn btn-default pull-right" name="history" value="viewOnMap">View on Map</button>' +
                        '</form>'
                }
                else {
                    vId = '<h5 class="foundData">No History Found</h5>'
                }
            });
            $(".deleteId").html(delId);
            $(".foundData").html(vId);
            $(".editId").html(editId);
            //$(".editFenceId").html(editGeoFenceId);
            $(".geoFence").html(geoFence);
        });


    });
});

$(".notselEditId").click(function () {
    alert('Please Select Any Vehicle')
});
//
//$(document).ready(function () {
//    $(document).on('mouseenter', '.editShow', function () {
//        $(this).find(":button").show();
//    }).on('mouseleave', '.editShow', function () {
//        $(this).find(":button").hide();
//    });
//});


//select map js

var mapType = 'map.setMapTypeId(google.maps.MapTypeId.TERRAIN);'
$("#select_map").change(function selectedMap() {
        var  stateIdHome = $( "#select_map" ).val();
        $(".selectedMapIdHome").html(stateIdHome);
        mapType = stateIdHome;

    }
);
