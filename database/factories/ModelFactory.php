<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
//
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Vehicle::class, function ($faker) {


    for ($i = 10; $i <= 99; $i++) {
        $imei = "0270436204" . $i;
    }
    return [
//        'vehicle_id' => $faker->randomDigit,
        'user_id' => $faker->randomElement($array = array('1', '1')),
        'vehicle_name' => $faker->name(),
        'registration_number' => $faker->randomDigit(),
        'imei_number' => $faker->unique()->numberBetween($min = 027043620410, $max = 027043620470),
//        'imei_number' => $faker->text($imei),
//        'sim_number' => $faker->$faker->randomDigit(10),
//        'speed_limit' => $faker->numberBetween(200,400),
//        'vehicle_status' => $faker->randomElement($array = array ('Active','Inactive')),
//        'current_lat' => $faker->latitude,
//        'current_long' => $faker->current_long,
//        'speed_limit' => $faker->longitude,
    ];

});

