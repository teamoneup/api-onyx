<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('vehicle_id');
            $table->integer('user_id')->unsigned();
            $table->string('vehicle_name',100);
            $table->string('registration_number',50);
            $table->string('imei_number',50);
            $table->string('sim_number',50);
            $table->string('speed_limit',100);
            $table->string('vehicle_status',20);
            $table->string('current_lat',100);
            $table->string('current_long',100);
            $table->string('update_location_date_time');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
