<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imei_number',50);
            $table->string('speed_limit',100);
            $table->date('last_date');
            $table->string('last_time',20);
            $table->string('latitude',100);
            $table->string('longitude',100);
            $table->string('vehicle_bearing',100);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_histories');
    }
}
