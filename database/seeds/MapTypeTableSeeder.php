<?php

use Flynsarmy\CsvSeeder\CsvSeeder;
class MapTypeTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = 'map_types';
        $this->filename = base_path().'/database/seeds/csvs/map_types.csv';
    }

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
//        DB::table($this->table)->truncate();

        parent::run();
    }
}
