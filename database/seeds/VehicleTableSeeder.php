<?php

use Flynsarmy\CsvSeeder\CsvSeeder;
class VehicleTableSeeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = 'vehicles';
        $this->filename = base_path().'/database/seeds/csvs/vehicles.csv';
    }

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
//        DB::table($this->table)->truncate();

        parent::run();
    }
}
