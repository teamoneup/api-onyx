<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'github' => [
        'client_id' => '9789525fb09090e0d531',
        'client_secret' => 'e6c3529b1496db594d943d099d1e7b39c4a4ed70',
        'redirect' => 'http://localhost/api-onyx/public/login/gitlogin',
    ],
    'google' => [
        'client_id' => '997331786376-er0lposi9gjhjstd3u8t5e9v7im14apl.apps.googleusercontent.com',
        'client_secret' => 'at-5-WzLQHWKVTb9evUI8Tve',
        'redirect' => 'http://128.199.232.1/login/googlelogin',
    ],

    'facebook' => [
        'client_id'     => '1624538634468190',
        'client_secret' => '69873275b439a9c225d3aa9875990585',
        'redirect'      => 'http://128.199.232.1/login/facebooklogin',
    ],

    'twitter' => [
        'client_id' => 'kL0sNV6cAbS0QzbpzPcqGyDfm',
        'client_secret' => 'JFBn90mm13f6jUFEHqjDuxOpm6cGyL8GPd8qMTT2eA6oh7itbt',
        'redirect' => 'http://128.199.232.1/login/twitterlogin',
    ] ,

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
];
