-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2015 at 09:39 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onyx`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'naveen', 'nvnsaini15@gmail.com', '$2y$10$tOi9ZVnEV/Dr8mBaoTuCYu9vMMmhWo70piN24V5y6IkNdEA2r115C', 'FyHuowyucl3YEVUi3sBu3wqceZGiNnbDTVsPcIjyEu27rHspukZ3cQg212fr', '2015-10-20 04:54:03', '2015-10-21 01:51:35'),
(3, 'riyaz', 'khanriyaz617@gmail.com', '$2y$10$41rSDi3FoSePqcDKK.v9UuCQtueBE6yrr3mzJcaUPMcGeQbCVhjP.', '7LqqDNtlUwj3q9596Yzg9F9lZzSDk9Q11exOgQiqc6v2D72EN0Knftjou0aP', '2015-10-20 23:33:44', '2015-10-21 01:33:47'),
(4, 'yuvraj', 'yuvraj@gmail.com', '$2y$10$DLEi7BsUTYVe7.b5EFR7SeVGDnU.qZY57DrV2Jp4JooRW7HmipzE2', 'FIccSNRHYRCt8LwkbvVdXyRluQ95hleBqB89ElkMCBtu7u1izTJpx88Bx6T4', '2015-10-21 01:15:06', '2015-10-21 01:52:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
