-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2015 at 09:40 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onyx`
--

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
  `vehicle_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `vehicle_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `registration_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `imei_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sim_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `speed_limit` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vehicle_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `current_lat` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `current_long` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vehicle_id`, `user_id`, `vehicle_name`, `registration_number`, `imei_number`, `sim_number`, `speed_limit`, `vehicle_status`, `current_lat`, `current_long`, `created_at`, `updated_at`) VALUES
(1, 3, 'Bolero', 'RJ 07 8888', '911350450515116', '123654789', '70', 'enable', '28.0696363291069', '74.61016130167991', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 'Scorpio', 'RJ 07 9999', '911313400050311', '321456987', '40', 'enable', '28.022064246053848', '73.31154298502952', '0000-00-00 00:00:00', '2015-10-21 01:58:07'),
(5, 2, 'Alto', 'PB 02 3477', '2315456', '11315645464', '80', 'enable', '31.524087', '75.906054', '2015-10-21 01:57:02', '2015-10-21 02:07:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`vehicle_id`), ADD KEY `vehicles_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `vehicle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `vehicles`
--
ALTER TABLE `vehicles`
ADD CONSTRAINT `vehicles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
